<?php

namespace App\Processor\Provider\Opta\Xml;

use App\Event\ResourceProcessed\Live\LiveMatchEvent;
use App\Processor\Provider\Traits\MatchTrait;
use AsResultados\OAMBundle\Api\Internal\Provider\Mapping\MappingInterface;
use AsResultados\OAMBundle\Api\Internal\Results\Category\CategoryInterface;
use AsResultados\OAMBundle\Api\Internal\Results\Match\Register as MatchRegister;
use AsResultados\OAMBundle\Exception\MissingItemException;
use AsResultados\OAMBundle\Exception\ValidationItemException;
use AsResultados\OAMBundle\Model\Collection\MappingCollection;
use AsResultados\OAMBundle\Model\Results\Match\Embed\Summoned;
use AsResultados\OAMBundle\Model\Results\Match\Embed\Team;
use AsResultados\OAMBundle\Model\Results\Match\Match;
use AsResultados\OAMBundle\Model\Collection\ProviderIdsCollection;
use AsResultados\OAMBundle\Exception\EmptyItemException;
use AsResultados\OAMBundle\Exception\MappingException;
use App\Processor\Provider\Traits\CompetitionSeasonIdTrait;
use App\Processor\Provider\Traits\MatchIdTrait;
use Exception;
use Symfony\Component\DomCrawler\Crawler;

class F31 extends AbstractProcessor
{
    use MatchIdTrait;
    use CompetitionSeasonIdTrait;
    use MatchTrait;

    private const DETAILS_ACCEPTED = array(
        CategoryInterface::MATCH_DETAIL_BASIC,
        CategoryInterface::MATCH_DETAIL_BASIC_GOALS,
    );

    /**
     * @var string
     */
    protected $matchProviderId;

    /**
     * @inheritDoc
     */
    protected function setProcessorVariablesFromMapping(): void
    {
        $mappingCollection = MappingCollection::getInstance();
        $this->setMatchId($mappingCollection->get(
            MappingInterface::ENTITY_MATCH, self::PROVIDER, $this->getMatchIdFromXml()
        ));
        $this->setMatchesDetails(
            $mappingCollection->getMultiple(
                MappingInterface::ENTITY_MATCH, self::PROVIDER
            ),
            $this->getClient()
        );
    }

    /**
     * @inheritDoc
     */
    public function run(): bool
    {
        $match = $this->getMatchFromXml();
        $matchRegister = MatchRegister::getInstance($this->getClient());
        //Update match
        try {
            //Patch instead of put because we do not change structure data (matchday, groupnumber, stage ... etc).
            $matchRegister->patch([$match]);
            $opUpdateMatch = true;
        } catch (EmptyItemException $e) {
            $this->getLogger()->info('Can not update match: ' . $e->getMessage());
        } catch (Exception $e) {
            $this->getLogger()->error('Can not update match: ' . $e->getMessage());
        }
        //Dispatch events
        if (isset($opUpdateMatch)) {
            $this->dispatchEvents($match);
        }
        return true;
    }

    /**
     * @param Match $match
     */
    private function dispatchEvents(Match $match): void
    {
        $event = new LiveMatchEvent();
        $event->setMatch($match->getId());
        $this->dispatcher->dispatch($event, $event->getEventName());
    }

    /**
     * @inheritDoc
     */
    protected function getMappingsFromProviderData(): array
    {
        $mappings = array();
        $mappings[] = $this->getMappingTeamsFromXml();
        $mappings[] = $this->getMappingPeopleFromXml();
        $mappings[] = $this->getMappingMatchFromXml();
        $mappings[] = new ProviderIdsCollection(MappingInterface::ENTITY_ROL, self::PROVIDER, true);
        $mappings[] = new ProviderIdsCollection(MappingInterface::ENTITY_TEAM_FORMATION, self::PROVIDER, true);
        return $mappings;
    }

    /**
     * @return Match
     * @throws Exception
     */
    protected function getMatchFromXml(): Match
    {
        if (!in_array($this->getMatches()[$this->getMatchId()]->getDetail()->getId(), self::DETAILS_ACCEPTED)) {
            throw new ValidationItemException('MATCH-SUMMONED', 'Match detail not accepted', $this->getMatches()[$this->getMatchId()]->getDetail()->getId());
        }
        $data = $this->getCrawledXmlDocument()->filterXPath('Lineup/@game_id');
        if ($data->count() !== 1) {
            throw new Exception('Match has no id');
        }
        $mappingCollection = MappingCollection::getInstance();
        $match = new Match();
        //If no mapping for match, we can not process the match
        //Opta send to us the match id without 'g' and we are saving this data with it
        $idMatch = 'g' . $data->text();
        if (!$mappingCollection->exists($mappingCollection::ENTITY_MATCH, self::PROVIDER, $idMatch)) {
            throw new MappingException(self::PROVIDER, $idMatch, $mappingCollection::ENTITY_MATCH);
        }
        $match->setId($mappingCollection->get($mappingCollection::ENTITY_MATCH, self::PROVIDER, $idMatch));
        //Detail
        $match->setDetailById(CategoryInterface::MATCH_DETAIL_BASIC_GOALS);
        //Teams
        $idLocal = $this->getCrawledXmlDocument()->filterXPath('Lineup/@home_team_id')->text();
        $idVisitor = $this->getCrawledXmlDocument()->filterXPath('Lineup/@away_team_id')->text();
        $this->getCrawledXmlDocument()->filterXPath('Lineup/Team')
            ->each(function (Crawler $dataXml) use ($mappingCollection, &$match, $idLocal, $idVisitor) {
                if ($dataXml->attr('id') === $idLocal) {
                    $match->setLocal($this->createMatchTeamFromXmlNode($dataXml));
                }
                if ($dataXml->attr('id') === $idVisitor) {
                    $match->setVisitor($this->createMatchTeamFromXmlNode($dataXml));
                }
            });
        return $match;
    }

    /**
     * @param Crawler $node
     * @return Team
     * @throws MappingException
     * @throws Exception
     */
    protected function createMatchTeamFromXmlNode(Crawler $node): Team
    {
        $mappingCollection = MappingCollection::getInstance();
        $team = new Team();
        //Opta send to us the team id without 't' and we are saving this data with it
        $idTeam = 't' . $node->attr('id');
        //Team id
        $team->setTeamById($mappingCollection->get(
            $mappingCollection::ENTITY_TEAM,
            self::PROVIDER,
            $idTeam
        ));
        //Summoned
        try {
            $data = $this->createMatchTeamSummonedFromXmlNode($node);
            if (!empty($data)) {
                $team->setSummoned($data);
            }
        } catch (Exception $e) {
            $this->getLogger()->warning('Can not insert summoned: ' . $e->getMessage());
        }
        //Formation
        if ($mappingCollection->exists(
            $mappingCollection::ENTITY_TEAM_FORMATION,
            self::PROVIDER,
            $node->attr('formation')
        )) {
            $team->setFormationById($mappingCollection->get(
                $mappingCollection::ENTITY_TEAM_FORMATION,
                self::PROVIDER,
                $node->attr('formation')
            ));
        }
        return $team;
    }

    /**
     * @param Crawler $node
     * @return Summoned[]
     */
    protected function createMatchTeamSummonedFromXmlNode(Crawler $node): array
    {
        $mappingCollection = MappingCollection::getInstance();
        $summoneds = array();
        $node->filterXPath('Team/Player')
            ->each(function (Crawler $dataXml) use ($mappingCollection, &$summoneds) {
                try {
                    $summoned = new Summoned();
                    //Person
                    //Opta send to us the player id without 'p' and we are saving this data with it
                    $idPerson = 'p' . $dataXml->attr('id');
                    $summoned->setPlayerById($mappingCollection->get(
                        $mappingCollection::ENTITY_PERSON,
                        self::PROVIDER,
                        $idPerson)
                    );
                    //Position
                    $position = $dataXml->attr('place');
                    $summoned->setPosition($position);
                    //Substitute
                    if ($position === '0') {
                        $summoned->setSubstitute(true);
                    } else {
                        $summoned->setSubstitute(false);
                    }
                    $summoneds[] = $summoned;
                } catch (MappingException $e) {
                    //Keep going, we won't insert that summoned
                    return;
                }
            });
        return $summoneds;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingMatchFromXml(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_MATCH, self::PROVIDER);
        $this->getCrawledXmlDocument()
            ->filterXPath('Lineup[@game_id]')
            ->each(function (Crawler $node) use ($collection) {
                //Opta send to us the match id without 'g' and we are saving this data with it
                $id = $node->attr('game_id');
                $id = 'g' . $id;
                $collection->addId($id);
            });
        return $collection;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingTeamsFromXml(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_TEAM, self::PROVIDER);
        $this->getCrawledXmlDocument()
            ->filterXPath('Lineup/Team[@id]')
            ->each(function (Crawler $node) use ($collection) {
                //Opta send to us the team id without 't' and we are saving this data with it
                $id = $node->attr('id');
                $id = 't' . $id;
                $collection->addId($id);
            });
        return $collection;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingPeopleFromXml(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_PERSON, self::PROVIDER);
        $this->getCrawledXmlDocument()
            ->filterXPath('Lineup/Team/Player[@id]')
            ->each(function (Crawler $node) use ($collection) {
                //Opta send to us the player id without 'p' and we are saving this data with it
                $id = 'p' . $node->attr('id');
                $collection->addId($id);
            });
        return $collection;
    }

    /**
     * @return string
     * @throws MissingItemException
     */
    protected function getMatchIdFromXml(): string
    {
        if (!isset($this->matchProviderId)) {
            $node = $this->getCrawledXmlDocument()->filterXPath('Lineup');
            if (is_null($node->attr('game_id'))) {
                throw new MissingItemException('game_id', 'xml node', 'provider xml file');
            }
            $this->matchProviderId = 'g' . $node->attr('game_id');
        }
        return $this->matchProviderId;
    }
}