<?php

namespace App\Processor\Provider\Opta\Xml;

use App\Processor\Provider\Traits\CompetitionSeasonsTrait;
use App\Utils\StringTrait;
use AsResultados\OAMBundle\Api\Internal\Provider\Mapping\MappingInterface;
use AsResultados\OAMBundle\Api\Internal\Results\RequestsResourceItem;
use AsResultados\OAMBundle\Api\Internal\Results\Venue\Register as VenueRegister;
use AsResultados\OAMBundle\Api\Internal\Results\VenueCompetitionSeason\Register as VenueCompetitionSeasonRegister;
use AsResultados\OAMBundle\Api\Internal\Results\VenueCompetitionSeason\Request as VenueCompetitionSeasonRequest;
use AsResultados\OAMBundle\Exception\DatabaseInconsistencyException;
use AsResultados\OAMBundle\Exception\MissingItemException;
use AsResultados\OAMBundle\Model\Collection\MappingCollection;
use AsResultados\OAMBundle\Model\Results\CompetitionSeason\CompetitionSeason;
use AsResultados\OAMBundle\Model\Results\Venue\Embed\Location;
use AsResultados\OAMBundle\Model\Results\VenueCompetitionSeason\Embed\Pitch;
use AsResultados\OAMBundle\Model\Results\Venue\Venue;
use AsResultados\OAMBundle\Model\Collection\Collection;
use AsResultados\OAMBundle\Model\Results\VenueCompetitionSeason\VenueCompetitionSeason;
use AsResultados\OAMBundle\Model\Collection\ProviderIdsCollection;
use AsResultados\OAMBundle\Exception\EmptyItemException;
use AsResultados\OAMBundle\Exception\MappingException;
use App\Processor\Provider\Traits\CompetitionSeasonIdTrait;
use Exception;
use Symfony\Component\DomCrawler\Crawler;

class F45 extends AbstractProcessor
{
    use CompetitionSeasonIdTrait;
    use CompetitionSeasonsTrait;
    use StringTrait;

    /**
     * Associates venues ids with the competitionSeasons in which they are used
     * @var string[]
     */
    protected $venuesInCompetitionSeasons;

    /**
     * @inheritDoc
     */
    protected function setProcessorVariablesFromMapping(): void
    {
        $mappingCollection = MappingCollection::getInstance();
        $this->setCompetitionSeasonId($mappingCollection->get(
            MappingInterface::ENTITY_COMPETITION_SEASON, self::PROVIDER, $this->getCompetitionSeasonFromXml()
        ));
        $this->setCompetitionSeasons([$this->getCompetitionSeasonId()], $this->getClient());
        $this->setMappingsOwn($this->getMappingsOwn());
    }

    /**
     * @inheritDoc
     */
    public function run(): bool
    {
        //Get venues
        $venues = $this->getVenuesFromXml();
        $venueRegister = VenueRegister::getInstance($this->getClient());
        //Insert venues
        try {
            $venueRegister->postWithMapping(
                $venues->getAllUnRegistered(), $venues->getUnRegisteredIds(), MappingInterface::ENTITY_VENUE, self::PROVIDER
            );
            $okInsertVenues = true;
            //Add new venues to collection
            $venues->removeAllUnRegistered();
            $venues->addMultipleRegistered($venueRegister->getLastInsertedItems());
        } catch (DatabaseInconsistencyException $e) {
            $this->getLogger()->emergency('Can not insert venues: ' . $e->getMessage());
            throw $e;
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not insert venues: ' . $e->getMessage());
        }
        //Get venuesCompetitionSeason
        $venuesCompetitionSeason = $this->getVenuesCompetitionSeasonFromXml();
        //Update venuesCompetitionSeason
        $venueCompetitionSeasonRegister = VenueCompetitionSeasonRegister::getInstance($this->getClient());
        try {
            $venueCompetitionSeasonRegister->patch($venuesCompetitionSeason->getAllRegistered());
            $opUpdateVenuesCompetitionSeason = true;
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not update venuesCompetitionSeason: ' . $e->getMessage());
        }
        //Insert venuesCompetitionSeason
        try {
            $venueCompetitionSeasonRegister->post($venuesCompetitionSeason->getAllUnRegistered());
            $opInsertVenuesCompetitionSeason = true;
            //Add new venuesCompetitionSeason to collection
            $venuesCompetitionSeason->removeAllUnRegistered();
            $venuesCompetitionSeason->addMultipleRegistered($venueCompetitionSeasonRegister->getLastInsertedItems());
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not insert venuesCompetitionSeason: ' . $e->getMessage());
        }
        //Update venuesCompetitionSeason inside venues
        $venuesCompetitionSeason->eachRegistered(function (VenueCompetitionSeason $venueCompetitionSeason) use (&$venues) {
            if ($venues->existsRegistered($venueCompetitionSeason->getVenue()->getId())) {
                $tmp = $venues->getRegistered($venueCompetitionSeason->getVenue()->getId());
                if ($tmp instanceof Venue) {
                    $competitionsSeasons = $tmp->getCompetitionsSeasons();
                    if (!is_array($competitionsSeasons)) {
                        $competitionsSeasons = array();
                    }
                    $competitionsSeasons[] = $venueCompetitionSeason->getId();
                    $tmp->setCompetitionsSeasons($competitionsSeasons);
                }
            }
        });
        //Update venues
        try {
            $venueRegister->patch($venues->getAllRegistered());
            $opUpdateVenues = true;
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not update venues: ' . $e->getMessage());
        }
        //Dispatch events
        if (isset($okInsertVenues) || isset($opUpdateVenues) ||
            isset($opInsertVenuesCompetitionSeason) || isset($opUpdateVenuesCompetitionSeason)) {
            $this->dispatchEvents();
        }
        return true;
    }

    private function dispatchEvents(): void
    {
    }

    /**
     * @inheritDoc
     */
    protected function getMappingsFromProviderData(): array
    {
        $mappings = array();
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_COMPETITION_SEASON, self::PROVIDER);
        $collection->addId($this->getCompetitionSeasonFromXml());
        $mappings[] = $collection;
        $mappings[] = $xmlMappingVenues = $this->getMappingVenuesFromXml();
        $mappings[] = $this->getMappingCountriesFromXml();
        return $mappings;
    }

    /**
     * @throws Exception
     */
    protected function getMappingsOwn(): array
    {
        $requests = array();
        $mappingCollection = MappingCollection::getInstance();
        $request = new VenueCompetitionSeasonRequest();
        foreach ($this->getCompetitionSeasons() as $competitionSeason) {
            $requests[] = new RequestsResourceItem(
                $request->getByVenuesAndCompetitionSeason(
                    $mappingCollection->getMultiple(MappingInterface::ENTITY_VENUE, self::PROVIDER),
                    $competitionSeason->getId()
                ),
                $request->getResource()
            );
        }
        return $requests;
    }

    /**
     * @return Collection
     */
    protected function getVenuesFromXml(): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $result = new Collection(Venue::class);
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/stadium')
            ->each(function (Crawler $node) use (&$result, $mappingCollection) {
                $id = $node->attr('uID');
                if (empty($id)) {
                    //No id, skip it
                    return;
                }
                $item = $this->createVenueFromXmlNode($node);
                if ($mappingCollection->exists($mappingCollection::ENTITY_VENUE, self::PROVIDER, $id)) {
                    $item->setId($mappingCollection->get($mappingCollection::ENTITY_VENUE, self::PROVIDER, $id));
                    //Set slug to null because it is already inserted
                    $item->setSlug(null);
                    $result->addRegistered($item);
                } else {
                    $result->addUnRegistered($item, $id);
                }
            });
        return $result;
    }

    /**
     * @param Crawler $node
     * @return Venue
     */
    protected function createVenueFromXmlNode(Crawler $node): Venue
    {
        $mappingCollection = MappingCollection::getInstance();
        $venue = new Venue();
        $venue->setSlug($this->normalizeString($node->attr('name')));
        $location = new Location();
        $node->filterXPath('stadium/address')->each(function (Crawler $nodeTmp) use ($venue, $location, $mappingCollection) {
            $data = $nodeTmp->attr('city');
            if (!empty($data)) {
                $location->setCity($data);
            }
            $data = $nodeTmp->attr('street');
            if (!empty($data)) {
                $location->setStreet($data);
            }
            if ($mappingCollection->exists(
                $mappingCollection::ENTITY_COUNTRY,
                self::PROVIDER,
                $nodeTmp->attr('country')
            )) {
                $location->setCountryById($mappingCollection->get(
                    $mappingCollection::ENTITY_COUNTRY,
                    self::PROVIDER,
                    $nodeTmp->attr('country')
                ));
            }
            $venue->setLocation($location);
        });
        $node->filterXPath('stadium/gps')->each(function (Crawler $nodeTmp) use ($location) {
            $location->setLatitude($nodeTmp->attr('latitude'));
            $location->setLongitude($nodeTmp->attr('longitude'));
        });
        return $venue;
    }

    /**
     * @return Collection
     */
    protected function getVenuesCompetitionSeasonFromXml(): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $result = new Collection(VenueCompetitionSeason::class);
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/stadium')
            ->each(function (Crawler $node) use (&$result, $mappingCollection) {
                $id = $node->attr('uID');
                if (empty($id)) {
                    //No id, skip it
                    return;
                }
                try {
                    $itemTmp = $this->createVenueCompetitionSeasonFromXmlNode($node);
                    $competitionSeasons = $this->getVenueCompetitionSeasons($itemTmp->getVenue()->getId());
                    foreach ($competitionSeasons as $competitionSeason) {
                        $item = clone($itemTmp);
                        $competitionSeasonObject = new CompetitionSeason();
                        $competitionSeasonObject->setId($competitionSeason);
                        $item->setCompetitionSeason($competitionSeasonObject);
                        if ($mappingCollection->existsOwn(VenueCompetitionSeason::class, $item->getUniqueIdByRelations())) {
                            $item->setId($mappingCollection->getOwn(VenueCompetitionSeason::class, $item->getUniqueIdByRelations()));
                            $result->addRegistered($item);
                        } else {
                            $result->addUnRegistered($item, $item->getUniqueIdByRelations());
                        }
                    }
                } catch (MissingItemException $e) {
                    //Skip item and keep going
                } catch (MappingException $e) {
                    //Mapping error, skip item and keep going
                    $this->getLogger()->warning('Can not create venueCompetitionSeason: ' . $e->getMessage());
                }
            });
        return $result;
    }

    /**
     * @param Crawler $node
     * @return VenueCompetitionSeason
     * @throws MappingException
     */
    protected function createVenueCompetitionSeasonFromXmlNode(Crawler $node): VenueCompetitionSeason
    {
        $mappingCollection = MappingCollection::getInstance();
        $venueCompetitionSeason = new VenueCompetitionSeason();
        $venueCompetitionSeason->setVenueById($mappingCollection->get(
            $mappingCollection::ENTITY_VENUE,
            self::PROVIDER,
            $node->attr('uID')
        ));
        $venueCompetitionSeason->setName($node->attr('name'));
        $venueCompetitionSeason->setCapacity($node->attr('capacity'));
        $node->filterXPath('stadium/pitch')->each(function (Crawler $nodeTmp) use ($venueCompetitionSeason) {
            $pitch = new Pitch();
            $pitch->setX($nodeTmp->attr('x'));
            $pitch->setY($nodeTmp->attr('y'));
            $venueCompetitionSeason->setPitch($pitch);
        });
        return $venueCompetitionSeason;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingVenuesFromXml(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_VENUE, self::PROVIDER);
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/stadium[@uID]')
            ->each(function (Crawler $node) use ($collection) {
                $collection->addId($node->attr('uID'));
            });
        return $collection;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingCountriesFromXml(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_COUNTRY, self::PROVIDER);
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/stadium/address[@country]')
            ->each(function (Crawler $node) use ($collection) {
                $collection->addId($node->attr('country'));
            });
        return $collection;
    }

    /**
     * @param string $id
     * @return string[]
     * @throws MissingItemException
     * @throws Exception
     */
    protected function getVenueCompetitionSeasons(string $id): array
    {
        if (!isset($this->venuesInCompetitionSeasons)) {
            $mappingCollection = MappingCollection::getInstance();
            $result = array();
            $this->getCrawledXmlDocument()
                ->filterXPath('SoccerFeed/SoccerDocument/stadium[@uID]')
                ->each(function (Crawler $node) use (&$result, $mappingCollection) {
                    if (!$mappingCollection->exists($mappingCollection::ENTITY_VENUE, self::PROVIDER, $node->attr('uID'))) {
                        //We do not have that mapping, very strange but keep going
                        return;
                    }
                    $id = $mappingCollection->get($mappingCollection::ENTITY_VENUE, self::PROVIDER, $node->attr('uID'));
                    if (!isset($result[$id][$this->getCompetitionSeasonId()])) {
                        $result[$id][$this->getCompetitionSeasonId()] = $this->getCompetitionSeasonId();
                    }
                });
            $this->venuesInCompetitionSeasons = $result;
        }
        if (!isset($this->venuesInCompetitionSeasons[$id])) {
            throw new MissingItemException($id, 'venuesInCompetitionSeasons', 'can not associate venue id to any competitionSeason');
        }
        return $this->venuesInCompetitionSeasons[$id];
    }
}