<?php

namespace App\Processor\Provider\Opta\Xml\RegisterItemsCompetitionSeason;

use App\Processor\Provider\Opta\Xml\AbstractProcessor;
use App\Processor\Provider\Traits\CompetitionSeasonIdTrait;
use App\Processor\Provider\Traits\CompetitionSeasonsTrait;
use App\Utils\StringTrait;
use AsResultados\OAMBundle\Api\Internal\Provider\Mapping\MappingInterface;
use AsResultados\OAMBundle\Api\Internal\Results\Category\CategoryInterface;
use AsResultados\OAMBundle\Api\Internal\Results\Person\Register as PersonRegister;
use AsResultados\OAMBundle\Api\Internal\Results\PersonCompetitionSeason\Register as PersonSeasonRegister;
use AsResultados\OAMBundle\Api\Internal\Results\PersonCompetitionSeason\Request as PersonCompetitionSeasonRequest;
use AsResultados\OAMBundle\Api\Internal\Results\RequestsResourceItem;
use AsResultados\OAMBundle\Api\Internal\Results\Venue\Register as VenueRegister;
use AsResultados\OAMBundle\Api\Internal\Results\VenueCompetitionSeason\Register as VenueSeasonRegister;
use AsResultados\OAMBundle\Api\Internal\Results\VenueCompetitionSeason\Request as VenueCompetitionSeasonRequest;
use AsResultados\OAMBundle\Exception\DatabaseInconsistencyException;
use AsResultados\OAMBundle\Exception\EmptyItemException;
use AsResultados\OAMBundle\Exception\MappingException;
use AsResultados\OAMBundle\Exception\MissingItemException;
use AsResultados\OAMBundle\Model\Collection\Collection;
use AsResultados\OAMBundle\Model\Collection\MappingCollection;
use AsResultados\OAMBundle\Model\Collection\ProviderIdsCollection;
use AsResultados\OAMBundle\Model\Results\CompetitionSeason\CompetitionSeason;
use AsResultados\OAMBundle\Model\Results\Person\Embed\Name;
use AsResultados\OAMBundle\Model\Results\PersonCompetitionSeason\PersonCompetitionSeason;
use AsResultados\OAMBundle\Model\Results\Person\Person;
use AsResultados\OAMBundle\Model\Results\Venue\Embed\Location;
use AsResultados\OAMBundle\Model\Results\Venue\Venue;
use AsResultados\OAMBundle\Model\Results\VenueCompetitionSeason\VenueCompetitionSeason;
use Exception;
use Symfony\Component\DomCrawler\Crawler;

class F1 extends AbstractProcessor
{
    use CompetitionSeasonIdTrait;
    use CompetitionSeasonsTrait;
    use StringTrait;

    /**
     * Associates referees ids with the competitionSeasons in which they played
     * @var string[]
     */
    protected $refereesInCompetitionSeasons;

    /**
     * Associates venues ids with the competitionSeasons in which they are used
     * @var string[]
     */
    protected $venuesInCompetitionSeasons;

    /**
     * @inheritDoc
     */
    protected function setProcessorVariablesFromMapping(): void
    {
        $mappingCollection = MappingCollection::getInstance();
        $stages = $mappingCollection->getMultiple(MappingInterface::ENTITY_STAGE, self::PROVIDER);
        //If we do not have stages mapping errors will be thrown to pending so we skip this
        if (!empty($stages)) {
            $this->setCompetitionSeasonsAndNumberOfMatchesInsertedAndDetailsByStageIds(
                $mappingCollection->getMultiple(MappingInterface::ENTITY_STAGE, self::PROVIDER),
                $this->getClient()
            );
        }
        $this->setMappingsOwn($this->getMappingsOwn());
    }

    /**
     * @inheritDoc
     */
    public function run(): bool
    {
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        /// Referees
        $people = $this->getRefereesFromXml();
        $personRegister = PersonRegister::getInstance($this->getClient());
        //We only do insert operations not update ones for person
        try {
            $personRegister->postWithMapping(
                $people->getAllUnRegistered(), $people->getUnRegisteredIds(), MappingInterface::ENTITY_PERSON, self::PROVIDER
            );
            //Add new people to collection
            $people->removeAllUnRegistered();
            $people->addMultipleRegistered($personRegister->getLastInsertedItems());
        } catch (DatabaseInconsistencyException $e) {
            $this->getLogger()->emergency('Can not insert people (referees): ' . $e->getMessage());
            throw $e;
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not insert people (referees): ' . $e->getMessage());
        }
        $personSeasonRegister = PersonSeasonRegister::getInstance($this->getClient());
        $peopleCompetitionSeason = $this->getRefereesCompetitionSeasonFromXml();
        //We only do insert operations not update ones for peopleCompetitionSeason
        try {
            $personSeasonRegister->post($peopleCompetitionSeason->getAllUnRegistered());
            $peopleCompetitionSeason->removeAllUnRegistered();
            $peopleCompetitionSeason->addMultipleRegistered($personSeasonRegister->getLastInsertedItems());
            $insertedPeopleCompetitionSeason = $personSeasonRegister->getLastInsertedItems();
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not insert peopleCompetitionSeason (referees): ' . $e->getMessage());
        }
        //Update peopleCompetitionSeason inside people only if we have inserted peopleCompetitionSeason
        if (isset($insertedPeopleCompetitionSeason) && !empty($insertedPeopleCompetitionSeason)) {
            $peopleCompetitionSeason->eachRegistered(function (PersonCompetitionSeason $personCompetitionSeason) use (&$people) {
                if ($people->existsRegistered($personCompetitionSeason->getPerson()->getId())) {
                    $tmp = $people->getRegistered($personCompetitionSeason->getPerson()->getId());
                    if ($tmp instanceof Person) {
                        $competitionsSeasons = $tmp->getCompetitionsSeasons();
                        if (!is_array($competitionsSeasons)) {
                            $competitionsSeasons = array();
                        }
                        $competitionsSeasons[] = $personCompetitionSeason;
                        $tmp->setCompetitionsSeasons($competitionsSeasons);
                    }
                }
            });
            //Update people to insert the competitionSeason
            try {
                $personRegister->patch($people->getAllRegistered());
            } catch (EmptyItemException $e) {
            } catch (Exception $e) {
                $this->getLogger()->error('Can not update people (referees): ' . $e->getMessage());
            }
        }
        /// Referees
        /////////////////////////////////////////////////////////////////////////////////////////////////////

        /////////////////////////////////////////////////////////////////////////////////////////////////////
        /// Venues
        $venues = $this->getVenuesFromXml();
        $venueRegister = VenueRegister::getInstance($this->getClient());
        //We only do insert operations not update ones for venue
        try {
            $venueRegister->postWithMapping(
                $venues->getAllUnRegistered(), $venues->getUnRegisteredIds(), MappingInterface::ENTITY_VENUE, self::PROVIDER
            );
            //Add new venues to collection
            $venues->removeAllUnRegistered();
            $venues->addMultipleRegistered($venueRegister->getLastInsertedItems());
        } catch (DatabaseInconsistencyException $e) {
            $this->getLogger()->emergency('Can not insert venues: ' . $e->getMessage());
            throw $e;
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not insert venues: ' . $e->getMessage());
        }
        $venueSeasonRegister = VenueSeasonRegister::getInstance($this->getClient());
        $venuesCompetitionSeason = $this->getVenuesCompetitionSeasonFromXml();
        //We only do insert operations not update ones for venueCompetitionSeason
        try {
            $venueSeasonRegister->post($venuesCompetitionSeason->getAllUnRegistered());
            $venuesCompetitionSeason->removeAllUnRegistered();
            $venuesCompetitionSeason->addMultipleRegistered($venueSeasonRegister->getLastInsertedItems());
            $insertedVenuesCompetitionSeason = $venueSeasonRegister->getLastInsertedItems();
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not insert venueCompetitionSeason: ' . $e->getMessage());
        }
        //Update venuesCompetitionSeason inside people only if we have inserted venuesCompetitionSeason
        if (isset($insertedVenuesCompetitionSeason) && !empty($insertedVenuesCompetitionSeason)) {
            $venuesCompetitionSeason->eachRegistered(function (VenueCompetitionSeason $venueCompetitionSeason) use (&$venues) {
                if ($venues->existsRegistered($venueCompetitionSeason->getVenue()->getId())) {
                    $tmp = $venues->getRegistered($venueCompetitionSeason->getVenue()->getId());
                    if ($tmp instanceof Venue) {
                        $competitionsSeasons = $tmp->getCompetitionsSeasons();
                        if (!is_array($competitionsSeasons)) {
                            $competitionsSeasons = array();
                        }
                        $competitionsSeasons[] = $venueCompetitionSeason->getId();
                        $tmp->setCompetitionsSeasons($competitionsSeasons);
                    }
                }
            });
            //Update venues
            try {
                $venueRegister->patch($venues->getAllRegistered());
            } catch (EmptyItemException $e) {
            } catch (Exception $e) {
                $this->getLogger()->error('Can not update venues: ' . $e->getMessage());
            }
        }
        /// Venues
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        return true;
    }

    /**
     * @inheritDoc
     */
    protected function getMappingsFromProviderData(): array
    {
        $mappings = array();
        $mappings[] = $this->getMappingStagesFromXml();
        $mappings[] = $this->getMappingRefereesFromXml();
        $mappings[] = $this->getMappingVenuesFromXml();
        return $mappings;
    }

    /**
     * @throws Exception
     */
    protected function getMappingsOwn(): array
    {
        $requests = array();
        $mappingCollection = MappingCollection::getInstance();
        $requestPerson = new PersonCompetitionSeasonRequest();
        $requestVenue = new VenueCompetitionSeasonRequest();
        foreach ($this->getCompetitionSeasons() as $competitionSeason) {
            $requests[] = new RequestsResourceItem(
                $requestPerson->getByPeopleAndCompetitionSeason(
                    $mappingCollection->getMultiple(MappingInterface::ENTITY_PERSON, self::PROVIDER),
                    $competitionSeason->getId()
                ),
                $requestPerson->getResource()
            );
            $requests[] = new RequestsResourceItem(
                $requestVenue->getByVenuesAndCompetitionSeason(
                    $mappingCollection->getMultiple(MappingInterface::ENTITY_VENUE, self::PROVIDER),
                    $competitionSeason->getId()
                ),
                $requestVenue->getResource()
            );
        }
        return $requests;
    }

    /**
     * @return Collection
     * @throws Exception
     */
    protected function getRefereesFromXml(): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $result = new Collection(Person::class);
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/MatchData/MatchOfficials/MatchOfficial')
            ->each(function (Crawler $node) use (&$result, $mappingCollection) {
                $id = $node->attr('uID');
                if (empty($id)) {
                    //No id, skip it
                    return;
                }
                $item = $this->createRefereeFromXmlNode($node);
                if ($mappingCollection->exists($mappingCollection::ENTITY_PERSON, self::PROVIDER, $id)) {
                    $item->setId($mappingCollection->get($mappingCollection::ENTITY_PERSON, self::PROVIDER, $id));
                    //Set slug to null because it is already inserted
                    $item->setSlug(null);
                    $result->addRegistered($item);
                } else {
                    $result->addUnRegistered($item, $id);
                }
            });
        return $result;
    }

    /**
     * @param Crawler $node
     * @return Person
     */
    protected function createRefereeFromXmlNode(Crawler $node): Person
    {
        $person = new Person();
        $name = new Name();
        $name->setFirst($node->attr('FirstName'));
        $name->setLast($node->attr('LastName'));
        $name->setKnown($node->attr('LastName'));
        $person->setName($name);
        $person->setSlug($this->normalizeString($name->getFirst() . ' ' . $name->getLast()));
        return $person;
    }

    /**
     * @return Collection
     */
    protected function getRefereesCompetitionSeasonFromXml(): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $result = new Collection(PersonCompetitionSeason::class);
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/MatchData/MatchOfficials/MatchOfficial')
            ->each(function (Crawler $node) use (&$result, $mappingCollection) {
                try {
                    $itemTmp = $this->createRefereeCompetitionSeasonFromXmlNode($node);
                    $competitionSeasons = $this->getRefereeCompetitionSeasons($itemTmp->getPerson()->getId());
                    foreach ($competitionSeasons as $competitionSeason) {
                        $item = clone($itemTmp);
                        $competitionSeasonObject = new CompetitionSeason();
                        $competitionSeasonObject->setId($competitionSeason);
                        $item->setCompetitionSeason($competitionSeasonObject);
                        if ($mappingCollection->existsOwn(PersonCompetitionSeason::class, $item->getUniqueIdByRelations())) {
                            $item->setId($mappingCollection->getOwn(PersonCompetitionSeason::class, $item->getUniqueIdByRelations()));
                            $result->addRegistered($item);
                        } else {
                            $result->addUnRegistered($item, $item->getUniqueIdByRelations());
                        }
                    }
                } catch (MappingException $e) {
                    //Mapping error, skip item and keep going
                }
            });
        return $result;
    }

    /**
     * @param Crawler $node
     * @return PersonCompetitionSeason
     * @throws MappingException
     */
    protected function createRefereeCompetitionSeasonFromXmlNode(Crawler $node): PersonCompetitionSeason
    {
        $mappingCollection = MappingCollection::getInstance();
        $personCompetitionSeason = new PersonCompetitionSeason();
        $personCompetitionSeason->setPersonById($mappingCollection->get(
            $mappingCollection::ENTITY_PERSON,
            self::PROVIDER,
            $node->attr('uID')
        ));
        $name = $node->attr('FirstName') . ' ' . $node->attr('LastName');
        $personCompetitionSeason->setName($name);
        $personCompetitionSeason->setTypeById(CategoryInterface::PERSON_TYPE_REFEREE);
        $personCompetitionSeason->setLoan(false);
        $personCompetitionSeason->setActive(true);
        return $personCompetitionSeason;
    }

    /**
     * @return Collection
     */
    protected function getVenuesFromXml(): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $result = new Collection(Venue::class);
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/MatchData/MatchInfo')
            ->each(function (Crawler $node) use (&$result, $mappingCollection) {
                try {
                    $id = $node->attr('Venue_id');
                    if (empty($id)) {
                        //No id, skip it
                        return;
                    }
                    $item = $this->createVenueFromXmlNode($node);
                    if ($mappingCollection->exists($mappingCollection::ENTITY_VENUE, self::PROVIDER, $id)) {
                        $item->setId($mappingCollection->get($mappingCollection::ENTITY_VENUE, self::PROVIDER, $id));
                        //Set slug to null because it is already inserted
                        $item->setSlug(null);
                        $result->addRegistered($item);
                    } else {
                        $result->addUnRegistered($item, $id);
                    }
                } catch (Exception $e) {
                    //Keep going, we do not set that venue
                }
            });
        return $result;
    }

    /**
     * @param Crawler $node
     * @return Venue
     * @throws MissingItemException
     */
    protected function createVenueFromXmlNode(Crawler $node): Venue
    {
        $venue = new Venue();
        $data = $node->parents()->filterXPath('MatchData/Stat[@Type="Venue"]');
        //If there is no name, we can not set slug, throw error
        if ($data->count() <= 0) {
            throw new MissingItemException('Stat[@Type="Venue"]', 'node in xml', 'provider xml');
        }
        if ($data->count() > 0) {
            $venue->setSlug($this->normalizeString($data->text()));
        }
        $location = new Location();
        $data = $node->parents()->filterXPath('MatchData/Stat[@Type="City"]');
        if ($data->count() > 0) {
            $location->setCity($data->text());
        }
        $venue->setLocation($location);
        return $venue;
    }

    /**
     * @return Collection
     */
    protected function getVenuesCompetitionSeasonFromXml(): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $result = new Collection(VenueCompetitionSeason::class);
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/MatchData/MatchInfo')
            ->each(function (Crawler $node) use (&$result, $mappingCollection) {
                try {
                    $itemTmp = $this->createVenueCompetitionSeasonFromXmlNode($node);
                    $competitionSeasons = $this->getVenueCompetitionSeasons($itemTmp->getVenue()->getId());
                    foreach ($competitionSeasons as $competitionSeason) {
                        $item = clone($itemTmp);
                        $competitionSeasonObject = new CompetitionSeason();
                        $competitionSeasonObject->setId($competitionSeason);
                        $item->setCompetitionSeason($competitionSeasonObject);
                        if ($mappingCollection->existsOwn(VenueCompetitionSeason::class, $item->getUniqueIdByRelations())) {
                            $item->setId($mappingCollection->getOwn(VenueCompetitionSeason::class, $item->getUniqueIdByRelations()));
                            $result->addRegistered($item);
                        } else {
                            $result->addUnRegistered($item, $item->getUniqueIdByRelations());
                        }
                    }
                } catch (MappingException $e) {
                    //Mapping error, skip item and keep going
                }
            });
        return $result;
    }

    /**
     * @param Crawler $node
     * @return VenueCompetitionSeason
     * @throws MappingException
     */
    protected function createVenueCompetitionSeasonFromXmlNode(Crawler $node): VenueCompetitionSeason
    {
        $mappingCollection = MappingCollection::getInstance();
        $venueCompetitionSeason = new VenueCompetitionSeason();
        $venueCompetitionSeason->setVenueById($mappingCollection->get(
            $mappingCollection::ENTITY_VENUE,
            self::PROVIDER,
            $node->attr('Venue_id')
        ));
        $data = $node->parents()->filterXPath('MatchData/Stat[@Type="Venue"]');
        if ($data->count() > 0) {
            $venueCompetitionSeason->setName($data->text());
        }
        return $venueCompetitionSeason;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingRefereesFromXml(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_PERSON, self::PROVIDER);
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/MatchData/MatchOfficials/MatchOfficial[@uID]')
            ->each(function (Crawler $node) use ($collection) {
                $collection->addId($node->attr('uID'));
            });
        return $collection;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingVenuesFromXml(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_VENUE, self::PROVIDER);
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/MatchData/MatchInfo[@Venue_id]')
            ->each(function (Crawler $node) use ($collection) {
                $collection->addId($node->attr('Venue_id'));
            });
        return $collection;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingStagesFromXml(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_STAGE, self::PROVIDER);
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/MatchData/MatchInfo')
            ->each(function (Crawler $node) use ($collection) {
                $stageId = $this->generateProviderStageId(
                    $this->getCompetitionSeasonFromXml(),
                    $node->attr('RoundType'),
                    $node->attr('RoundNumber'));
                $collection->addId($stageId);
            });
        return $collection;
    }

    /**
     * @param string $id
     * @return string[]
     * @throws MissingItemException
     * @throws Exception
     */
    protected function getVenueCompetitionSeasons(string $id): array
    {
        if (!isset($this->venuesInCompetitionSeasons)) {
            $mappingCollection = MappingCollection::getInstance();
            $result = array();
            $this->getCrawledXmlDocument()
                ->filterXPath('SoccerFeed/SoccerDocument/MatchData/MatchInfo')
                ->each(function (Crawler $node) use (&$result, $mappingCollection) {
                    $stageId = $this->generateProviderStageId(
                        $this->getCompetitionSeasonFromXml(),
                        $node->attr('RoundType'),
                        $node->attr('RoundNumber'));
                    if (!$mappingCollection->exists(
                        MappingInterface::ENTITY_STAGE,
                        self::PROVIDER,
                        $stageId)) {
                        //Do not have mapping for the stage, very strange at this point, but keep going (F1 will log this)
                        return;
                    }
                    $competitionSeasonId = $this->getCompetitionSeasonIdFromStageId(
                        $mappingCollection->get(
                            MappingInterface::ENTITY_STAGE,
                            self::PROVIDER,
                            $stageId)
                    );
                    $id = $node->attr('Venue_id');
                    if (empty($id)) {
                        //This matchInfo does not contain a venue, keep going
                        return;
                    }
                    if (!$mappingCollection->exists($mappingCollection::ENTITY_VENUE, self::PROVIDER, $id)) {
                        //We do not have this venue mapping, very strange but keep going
                        return;
                    }
                    $id = $mappingCollection->get($mappingCollection::ENTITY_VENUE, self::PROVIDER, $id);
                    if (!isset($result[$id][$competitionSeasonId])) {
                        $result[$id][$competitionSeasonId] = $competitionSeasonId;
                    }
                });
            if (empty($result)) {
                throw new Exception('There are not venues in calendar provider file');
            }
            $this->venuesInCompetitionSeasons = $result;
        }
        if (!isset($this->venuesInCompetitionSeasons[$id])) {
            throw new MissingItemException($id, 'venuesInCompetitionSeasons', 'can not associate venue id to any competitionSeason');
        }
        return $this->venuesInCompetitionSeasons[$id];
    }

    /**
     * @param string $id
     * @return string[]
     * @throws MissingItemException
     * @throws Exception
     */
    protected function getRefereeCompetitionSeasons(string $id): array
    {
        if (!isset($this->refereesInCompetitionSeasons)) {
            $mappingCollection = MappingCollection::getInstance();
            $result = array();
            $this->getCrawledXmlDocument()
                ->filterXPath('SoccerFeed/SoccerDocument/MatchData/MatchInfo')
                ->each(function (Crawler $node) use (&$result, $mappingCollection) {
                    $stageId = $this->generateProviderStageId(
                        $this->getCompetitionSeasonFromXml(),
                        $node->attr('RoundType'),
                        $node->attr('RoundNumber'));
                    if (!$mappingCollection->exists(
                        MappingInterface::ENTITY_STAGE,
                        self::PROVIDER,
                        $stageId)) {
                        //Do not have mapping for the stage, very strange at this point, but keep going (F1 will log this)
                        return;
                    }
                    $competitionSeasonId = $this->getCompetitionSeasonIdFromStageId(
                        $mappingCollection->get(
                            MappingInterface::ENTITY_STAGE,
                            self::PROVIDER,
                            $stageId)
                    );
                    $node->parents()->filterXPath('MatchData/MatchOfficials/MatchOfficial[@uID]')->each(function (Crawler $nodeReferee) use (&$result, $mappingCollection, $competitionSeasonId) {
                        if (!$mappingCollection->exists($mappingCollection::ENTITY_PERSON, self::PROVIDER, $nodeReferee->attr('uID'))) {
                            //We do not have this referee mapping, very strange but keep going
                            return;
                        }
                        $id = $mappingCollection->get($mappingCollection::ENTITY_PERSON, self::PROVIDER, $nodeReferee->attr('uID'));
                        if (!isset($result[$id][$competitionSeasonId])) {
                            $result[$id][$competitionSeasonId] = $competitionSeasonId;
                        }
                    });
                });
            if (empty($result)) {
                throw new Exception('There are not referees in calendar provider file');
            }
            $this->refereesInCompetitionSeasons = $result;
        }
        if (!isset($this->refereesInCompetitionSeasons[$id])) {
            throw new MissingItemException($id, 'refereesInCompetitionSeasons', 'can not associate referee id to any competitionSeason');
        }
        return $this->refereesInCompetitionSeasons[$id];
    }
}