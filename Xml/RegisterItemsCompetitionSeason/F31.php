<?php

namespace App\Processor\Provider\Opta\Xml\RegisterItemsCompetitionSeason;

use App\Processor\Provider\Opta\Xml\AbstractProcessor;
use App\Processor\Provider\Traits\CompetitionSeasonIdTrait;
use AsResultados\OAMBundle\Api\Internal\Provider\Mapping\MappingInterface;
use AsResultados\OAMBundle\Api\Internal\Results\Category\CategoryInterface;
use AsResultados\OAMBundle\Api\Internal\Results\Person\Register as PersonRegister;
use AsResultados\OAMBundle\Api\Internal\Results\PersonCompetitionSeason\Register as PersonSeasonRegister;
use AsResultados\OAMBundle\Api\Internal\Results\PersonCompetitionSeason\Request as PersonCompetitionSeasonRequest;
use AsResultados\OAMBundle\Api\Internal\Results\RequestsResourceItem;
use AsResultados\OAMBundle\Exception\EmptyItemException;
use AsResultados\OAMBundle\Exception\MappingException;
use AsResultados\OAMBundle\Exception\MissingItemException;
use AsResultados\OAMBundle\Model\Collection\Collection;
use AsResultados\OAMBundle\Model\Collection\MappingCollection;
use AsResultados\OAMBundle\Model\Collection\ProviderIdsCollection;
use AsResultados\OAMBundle\Model\Results\Person\Person;
use AsResultados\OAMBundle\Model\Results\PersonCompetitionSeason\PersonCompetitionSeason;
use Exception;
use Symfony\Component\DomCrawler\Crawler;


class F31 extends AbstractProcessor
{
    use CompetitionSeasonIdTrait;

    /**
     * @inheritDoc
     */
    protected function setProcessorVariablesFromMapping(): void
    {
        $mappingCollection = MappingCollection::getInstance();
        $this->setCompetitionSeasonId($mappingCollection->get(
            MappingInterface::ENTITY_COMPETITION_SEASON, self::PROVIDER, $this->getCompetitionSeasonFromXml()
        ));
        $this->setMappingsOwn($this->getMappingsOwn());
    }

    /**
     * @inheritDoc
     */
    public function run(): bool
    {
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        /// Players
        $personSeasonRegister = PersonSeasonRegister::getInstance($this->getClient());
        $peopleCompetitionSeason = $this->getPeoplePlayerCompetitionsSeasonsFromXml();
        //Insert peopleCompetitionSeason (we only do insert operations)
        try {
            $personSeasonRegister->post($peopleCompetitionSeason->getAllUnRegistered());
            //Add new peopleCompetitionSeason who are players to collection
            $peopleCompetitionSeason->removeAllUnRegistered();
            $peopleCompetitionSeason->addMultipleRegistered($personSeasonRegister->getLastInsertedItems());
            $insertedPeopleCompetitionSeason = $personSeasonRegister->getLastInsertedItems();
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not insert peopleCompetitionSeason (players): ' . $e->getMessage());
        }
        $personRegister = PersonRegister::getInstance($this->getClient());
        //Update peopleCompetitionSeason inside people to update competitionSeasons
        if (isset($insertedPeopleCompetitionSeason) && !empty($insertedPeopleCompetitionSeason)) {
            $people = $this->createPeopleFromPeopleCompetitionSeason($insertedPeopleCompetitionSeason);
            try {
                $personRegister->patch($people);
            } catch (EmptyItemException $e) {
            } catch (Exception $e) {
                $this->getLogger()->error('Can not update people (players): ' . $e->getMessage());
            }
        }
        /// Players
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        return true;
    }

    /**
     * @inheritDoc
     */
    protected function getMappingsFromProviderData(): array
    {
        $mappings = array();
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_COMPETITION_SEASON, self::PROVIDER);
        $collection->addId($this->getCompetitionSeasonFromXml());
        $mappings[] = $collection;
        $mappings[] = $this->getMappingTeamsFromXml();
        $mappings[] = $this->getMappingPersonsFromXml();
        return $mappings;
    }

    /**
     * @throws Exception
     */
    protected function getMappingsOwn(): array
    {
        $requests = array();
        $mappingCollection = MappingCollection::getInstance();
        $requestPerson = new PersonCompetitionSeasonRequest();
        $requests[] = new RequestsResourceItem(
            $requestPerson->getByPeopleAndCompetitionSeason(
                $mappingCollection->getMultiple(MappingInterface::ENTITY_PERSON, self::PROVIDER),
                $this->getCompetitionSeasonId()
            ),
            $requestPerson->getResource()
        );
        return $requests;
    }

    /**
     * @return Collection
     */
    protected function getPeoplePlayerCompetitionsSeasonsFromXml(): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $result = new Collection(PersonCompetitionSeason::class);
        $this->getCrawledXmlDocument()
            ->filterXPath('Lineup/Team/Player')
            ->each(function (Crawler $node) use (&$result, $mappingCollection) {
                try {
                    $item = $this->createPeopleCompetitionsSeasonsFromXmlNode($node);
                    if ($mappingCollection->existsOwn(PersonCompetitionSeason::class, $item->getUniqueIdByRelations())) {
                        $item->setId($mappingCollection->getOwn(PersonCompetitionSeason::class, $item->getUniqueIdByRelations()));
                        $result->addRegistered($item);
                    } else {
                        $result->addUnRegistered($item, $item->getUniqueIdByRelations());
                    }
                } catch (MissingItemException $e) {
                    //Skip item and keep going
                } catch (MappingException $e) {
                    //Mapping error, skip item and keep going
                }
            });
        return $result;
    }

    /**
     * @param Crawler $node
     * @return PersonCompetitionSeason
     * @throws MappingException
     */
    protected function createPeopleCompetitionsSeasonsFromXmlNode(Crawler $node): PersonCompetitionSeason
    {
        $mappingCollection = MappingCollection::getInstance();
        $personCompetitionSeason = new PersonCompetitionSeason();
        //The provider is sending the player id without the initial p so we add it manually
        $personCompetitionSeason->setPersonById($mappingCollection->get(
            $mappingCollection::ENTITY_PERSON,
            self::PROVIDER,
            'p' . $node->attr('id')
        ));
        //The provider is sending the team id without the initial t so we add it manually
        $personCompetitionSeason->setTeamById($mappingCollection->get(
            $mappingCollection::ENTITY_TEAM,
            self::PROVIDER,
            't' . $node->parents()->attr('id')
        ));
        $personCompetitionSeason->setCompetitionSeasonById($this->getCompetitionSeasonId());
        $personCompetitionSeason->setTypeById(CategoryInterface::PERSON_TYPE_PLAYER);
        $personCompetitionSeason->setName($node->attr('first_name') . ' ' . $node->attr('last_name'));
        //We do not know loan attribute but we need to insert in the api, so ...
        $personCompetitionSeason->setLoan(false);
        $personCompetitionSeason->setActive(true);
        return $personCompetitionSeason;
    }

    /**
     * @param PersonCompetitionSeason[] $peopleCompetitionSeason
     * @return Person[]
     */
    protected function createPeopleFromPeopleCompetitionSeason(array $peopleCompetitionSeason): array
    {
        $result = array();
        foreach ($peopleCompetitionSeason as $personCompetitionSeason) {
            $person = new Person();
            $person->setId($personCompetitionSeason->getPerson()->getId());
            $person->setCompetitionsSeasons([$personCompetitionSeason]);
            $result[] = $person;
        }
        return $result;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingTeamsFromXml(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_TEAM, self::PROVIDER);
        $this->getCrawledXmlDocument()
            ->filterXPath('Lineup/Team[@id]')
            ->each(function (Crawler $node) use ($collection) {
                //The provider is sending the team id without the initial t so we add it manually
                $collection->addId('t' . $node->attr('id'));
            });
        return $collection;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingPersonsFromXml(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_PERSON, self::PROVIDER);
        $this->getCrawledXmlDocument()
            ->filterXPath('Lineup/Team/Player[@id]')
            ->each(function (Crawler $node) use ($collection) {
                //The provider is sending the player id without the initial p so we add it manually
                $collection->addId('p' . $node->attr('id'));
            });
        return $collection;
    }

    /**
     * @inheritDoc
     */
    protected function getCompetitionFromXml(): string
    {
        if (!isset($this->competitionProviderId)) {
            $node = $this->getCrawledXmlDocument()->filterXPath('Lineup');
            if (is_null($node->attr('competition_id'))) {
                throw new MissingItemException('competition_id', 'xml node', 'provider xml file');
            }
            $this->competitionProviderId = (string)$node->attr('competition_id');
        }
        return $this->competitionProviderId;
    }

    /**
     * @inheritDoc
     */
    protected function getSeasonFromXml(): string
    {
        if (!isset($this->seasonProviderId)) {
            $node = $this->getCrawledXmlDocument()->filterXPath('Lineup');
            if (is_null($node->attr('season_id'))) {
                throw new MissingItemException('season_id', 'xml node', 'provider xml file');
            }
            $this->seasonProviderId = (string)$node->attr('season_id');
        }
        return $this->seasonProviderId;
    }
}