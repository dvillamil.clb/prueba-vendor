<?php

namespace App\Processor\Provider\Opta\Xml;

use AsResultados\OAMBundle\Api\Internal\Provider\Mapping\MappingInterface;
use AsResultados\OAMBundle\Api\Internal\Provider\Mapping\Resource as MappingResource;
use AsResultados\OAMBundle\Api\Internal\Results\Classification\Register as ClassificationRegister;
use AsResultados\OAMBundle\Model\Results\CompetitionSeason\CompetitionSeasonStructure;
use AsResultados\OAMBundle\Model\Collection\MappingCollection;
use AsResultados\OAMBundle\Model\Results\Classification\Classification;
use AsResultados\OAMBundle\Model\Results\Classification\Embed\Data;
use AsResultados\OAMBundle\Model\Results\Classification\Embed\Goals;
use AsResultados\OAMBundle\Model\Results\Classification\Embed\MatchsInfo;
use AsResultados\OAMBundle\Api\Internal\Results\Classification\Request as ClassificationRequest;
use AsResultados\OAMBundle\Api\Internal\Results\Classification\Resource as ClassificationResource;
use AsResultados\OAMBundle\Model\Results\Classification\Embed\Position;
use AsResultados\OAMBundle\Model\Collection\Collection;
use AsResultados\OAMBundle\Model\Collection\ProviderIdsCollection;
use App\Event\ResourceProcessed\Classification\ClassificationEvent;
use AsResultados\OAMBundle\Exception\EmptyItemException;
use App\Processor\Provider\Traits\CompetitionSeasonTrait;
use App\Processor\Provider\Traits\StageIdTrait;
use Exception;
use Symfony\Component\DomCrawler\Crawler;
use Throwable;

class F3 extends AbstractProcessor
{
    use StageIdTrait;
    use CompetitionSeasonTrait;

    /**
     * @var string
     */
    private $stageProviderId;

    /**
     * @inheritDoc
     */
    protected function setProcessorVariablesFromMapping(): void
    {
        $mappingCollection = MappingCollection::getInstance();
        $this->setCompetitionSeason(
            $mappingCollection->get(
                MappingInterface::ENTITY_COMPETITION_SEASON, self::PROVIDER, $this->getCompetitionSeasonFromXml()
            ),
            $this->getClient()
        );
        $this->setStageId($mappingCollection->get(
            MappingInterface::ENTITY_STAGE, self::PROVIDER, $this->getStageFromXml()
        ));
    }

    /**
     * @inheritDoc
     */
    public function run(): bool
    {
        $classifications = $this->getClassificationsFromXml();
        $classificationRegister = ClassificationRegister::getInstance($this->getClient());
        //Check structure
        try {
            $structure = new CompetitionSeasonStructure($this->getCompetitionSeason());
            $structure->checkClassificationsStructure(
                array_merge($classifications->getAllUnRegistered(), $classifications->getAllRegistered())
            );
        } catch (Exception $e) {
            $this->getLogger()->critical(
                'Classifications do not pass checking structure process: ' . $e->getMessage(),
                [$this->getLogger()::CHANNEL_EDITORIAL]
            );
            throw $e;
        }
        //Update
        try {
            $classificationRegister->patch($classifications->getAllRegistered());
            $opUpdateClassifications = true;
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not update classifications: ' . $e->getMessage());
        }
        //Insert
        try {
            $classificationRegister->post($classifications->getAllUnRegistered());
            $opInsertClassifications = true;
            //Add new classifications to collection
            $classifications->removeAllUnRegistered();
            $classifications->addMultipleRegistered($classificationRegister->getLastInsertedItems());
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not insert classifications: ' . $e->getMessage());
        }
        //Dispatch events
        if (isset($opInsertClassifications) || isset($opUpdateClassifications)) {
            $this->dispatchEvents($classifications->getAllRegistered());
        }
        return true;
    }

    /**
     * @param Classification[] $classifications
     */
    private function dispatchEvents(array $classifications): void
    {
        foreach ($classifications as $classification) {
            $event = new ClassificationEvent();
            $event->setCompetitionSeason($this->getCompetitionSeason()->getId());
            $event->setStage($classification->getStage()->getId());
            $event->setMatchDay($classification->getMatchDay());
            $this->dispatcher->dispatch($event, $event->getEventName());
        }
    }

    /**
     * @inheritDoc
     */
    protected function getMappingsFromProviderData(): array
    {
        $mappings = array();
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_COMPETITION_SEASON, self::PROVIDER);
        $collection->addId($this->getCompetitionSeasonFromXml());
        $mappings[] = $collection;
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_STAGE, self::PROVIDER);
        $collection->addId($this->getStageFromXml());
        $mappings[] = $collection;
        $mappings[] = $this->getMappingTeamsFromXml();
        $mappings[] = new ProviderIdsCollection(MappingInterface::ENTITY_CONFERENCE, self::PROVIDER, true);
        return $mappings;
    }

    /**
     * @return Collection
     * @throws Exception
     */
    protected function getClassificationsFromXml(): Collection
    {
        $classificationArray = array();
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/Competition/TeamStandings')
            ->each(function (Crawler $nodeClassification) use (&$classificationArray) {
                $classificationArray[] = $this->createClassificationFromXmlNode($nodeClassification);
            });
        $classificationArray = $this->getRegisteredItemsCollectionFromArray($classificationArray);
        return $classificationArray;
    }

    /**
     * @param Crawler $node
     * @return Classification
     * @throws Exception
     */
    protected function createClassificationFromXmlNode(Crawler $node): Classification
    {
        $mappingCollection = MappingCollection::getInstance();
        $mappingResource = new MappingResource();
        $dataArray = array();
        $classification = new Classification();
        $classification->setStageById($this->getStageId());
        $classification->setCompetitionSeasonById($this->getCompetitionSeason()->getId());
        //////////////////////////////////////////////////////
        //MatchDay
        //Searching for the first match day of that stage
        $firstMatchDayStage = null;
        $roundNode = $this->getCrawledXmlDocument()->filterXPath('SoccerFeed/SoccerDocument[@Round]');
        if ($roundNode->count() > 0) {
            $round = $roundNode->attr('Round');
        }
        if (isset($round)) {
            $queryTmp = 'MatchInfo[@RoundNumber="' . $round . '"]';
            $query = '//' . $queryTmp . '/@MatchDay[not(. > ../' . $queryTmp . '/@MatchDay)][1]';
            $firstMatchDayStage = $this->getCrawledXmlDocument(
                $this->getFileSystemCalendarPath(
                    $this->getCompetitionFromXml(),
                    $this->getSeasonFromXml()
                )
            )->filterXPath($query);
            if ($firstMatchDayStage->count() > 0) {
                $firstMatchDayStage = $firstMatchDayStage->text();
            }
        }
        $classification->setMatchDay(
            $this->getRealMatchDay($node->attr('Matchday'), $firstMatchDayStage)
        );
        //MatchDay
        //////////////////////////////////////////////////////

        //////////////////////////////////////////////////////
        /// Group or conference
        $group = $node->filterXPath('TeamStandings/Round/Name');
        if ($group->count() > 0 && !empty($group->attr('id'))) {
            if ($mappingCollection->exists(
                $mappingResource::ENTITY_CONFERENCE,
                self::PROVIDER,
                $group->attr('id')
            )) {
                $classification->setConferenceById($mappingCollection->get(
                    $mappingResource::ENTITY_CONFERENCE,
                    self::PROVIDER,
                    $group->attr('id')
                ));
            } else {
                $classification->setGroupNumber($this->generateGroupNumber($group->attr('id')));
            }
        }
        /// Group or conference
        //////////////////////////////////////////////////////

        $node->filterXPath('TeamStandings/TeamRecord')
            ->each(function (Crawler $nodeTeam) use (&$dataArray, $mappingCollection, $mappingResource) {
                $data = new Data();
                $data->setTeamById($mappingCollection->get(
                    $mappingResource::ENTITY_TEAM,
                    self::PROVIDER,
                    $nodeTeam->attr('TeamRef')));
                $data->setTotal($this->createClassificationMatchsInfoFromXmlNode($nodeTeam));
                $data->setHome($this->createClassificationMatchsInfoFromXmlNode($nodeTeam, 'home'));
                $data->setAway($this->createClassificationMatchsInfoFromXmlNode($nodeTeam, 'away'));
                $dataArray[] = $data;
            });
        $classification->setData($dataArray);
        return $classification;
    }

    /**
     * @param Crawler $nodeTeam
     * @param string|null $type
     * @return MatchsInfo
     */
    protected function createClassificationMatchsInfoFromXmlNode(Crawler $nodeTeam, string $type = null): MatchsInfo
    {
        if (is_null($type)) {
            $type = '';
        } else {
            $type = ucfirst($type);
        }
        $matchsInfo = new MatchsInfo();
        $data = $nodeTeam->filterXPath('TeamRecord/Standing/' . $type . 'Points');
        if ($data->count() > 0) {
            $matchsInfo->setPoints($data->text());
        }
        $data = $nodeTeam->filterXPath('TeamRecord/Standing/' . $type . 'Played');
        if ($data->count() > 0) {
            $matchsInfo->setPlayed($data->text());
        }
        $data = $nodeTeam->filterXPath('TeamRecord/Standing/' . $type . 'Won');
        if ($data->count() > 0) {
            $matchsInfo->setWon($data->text());
        }
        $data = $nodeTeam->filterXPath('TeamRecord/Standing/' . $type . 'Drawn');
        if ($data->count() > 0) {
            $matchsInfo->setDrawn($data->text());
        }
        $data = $nodeTeam->filterXPath('TeamRecord/Standing/' . $type . 'Lost');
        if ($data->count() > 0) {
            $matchsInfo->setLost($data->text());
        }
        $position = new Position();
        $data = $nodeTeam->filterXPath('TeamRecord/Standing/' . $type . 'Position');
        if ($data->count() > 0) {
            $position->setNumber($data->text());
        }
        $data = $nodeTeam->filterXPath('TeamRecord/Standing/' . $type . 'StartDayPosition');
        if ($data->count() > 0 && (int)$data->text() !== 0 && !is_null($position->getNumber())) {
            $position->setChange((int)$data->text() - $position->getNumber());
        }
        $matchsInfo->setPosition($position);
        $goals = new Goals();
        $data = $nodeTeam->filterXPath('TeamRecord/Standing/' . $type . 'For');
        if ($data->count() > 0) {
            $goals->setFor($data->text());
        }
        $data = $nodeTeam->filterXPath('TeamRecord/Standing/' . $type . 'Against');
        if ($data->count() > 0) {
            $goals->setAgainst($data->text());
        }
        $matchsInfo->setGoals($goals);
        return $matchsInfo;
    }

    /**
     * @param Classification[] $classifications
     * @return Collection
     * @throws Exception
     */
    protected final function getRegisteredItemsCollectionFromArray(array $classifications): Collection
    {
        $classificationRequest = new ClassificationRequest();
        $requests = array();
        foreach ($classifications as $classification) {
            $request = $classificationRequest->getByStageAndMatchDayAndGroupAndConference(
                $classification->getStage()->getId(),
                $classification->getMatchDay(),
                $classification->getGroupNumber(),
                (!is_null($classification->getConference()) ? $classification->getConference()->getId() : null)
            );
            $this->getClient()->addRequest($request);
            $requests[$classification->getUniqueIdByRelations()] = $request;
        }
        $this->getClient()->execute(true);
        $classificationResource = new ClassificationResource();
        $classificationCollection = new Collection(Classification::class);
        foreach($classifications as $classification) {
            try {
                $response = $this->getClient()->getResponseFor($requests[$classification->getUniqueIdByRelations()], [$classificationRequest::STATUS_CODE_OK]);
                if (!isset($response[$classificationResource->getResponseItemsName()][0])) {
                    throw new Exception('Classification not found');
                }
                $classificationObj = $classificationResource->deserializeOne(
                    $response[$classificationResource->getResponseItemsName()][0]
                );
                $classification->setId($classificationObj->getId());
                $classificationCollection->addRegistered($classification);
            } catch (Throwable $e) {
                //We haven't found that classification
                $classificationCollection->addUnRegistered($classification, null);
            }
        }
        return $classificationCollection;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingTeamsFromXml(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_TEAM, self::PROVIDER);
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/Competition/TeamStandings/TeamRecord[@TeamRef] 
                            | SoccerFeed/SoccerDocument/Competition/TeamStandings/Round/TeamRecord[@TeamRef]')
            ->each(function (Crawler $node) use ($collection) {
                $collection->addId($node->attr('TeamRef'));
            });
        return $collection;
    }

    /**
     * @return string
     * @throws Exception
     */
    protected function getStageFromXml(): string
    {
        if (!isset($this->stageProviderId)) {
            $node = $this->getCrawledXmlDocument()->filterXPath('SoccerFeed/SoccerDocument');
            $this->stageProviderId = $this->generateProviderStageId(
                $this->generateProviderCompetitionSeasonId(
                    $node->attr('competition_id'),
                    $node->attr('season_id')
                ),
                null,
                $node->attr('Round')
            );
        }
        return $this->stageProviderId;
    }
}