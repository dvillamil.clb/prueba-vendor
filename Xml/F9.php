<?php

namespace App\Processor\Provider\Opta\Xml;

use App\Event\ResourceProcessed\Live\LiveMatchEvent;
use AsResultados\OAMBundle\Api\Internal\Provider\Mapping\MappingInterface;
use AsResultados\OAMBundle\Api\Internal\Results\Category\CategoryInterface;
use AsResultados\OAMBundle\Api\Internal\Results\MatchTeamStat\Request as MatchTeamStatRequest;
use AsResultados\OAMBundle\Api\Internal\Results\MatchTeamStat\Register as MatchTeamStatRegister;
use AsResultados\OAMBundle\Api\Internal\Results\MatchPersonStat\Request as MatchPersonStatRequest;
use AsResultados\OAMBundle\Api\Internal\Results\MatchPersonStat\Register as MatchPersonStatRegister;
use AsResultados\OAMBundle\Api\Internal\Results\Match\Register as MatchRegister;
use AsResultados\OAMBundle\Api\Internal\Results\RequestsResourceItem;
use AsResultados\OAMBundle\Exception\MissingItemException;
use AsResultados\OAMBundle\Model\Collection\Collection;
use AsResultados\OAMBundle\Model\Collection\MappingCollection;
use AsResultados\OAMBundle\Model\Results\Category\Category;
use AsResultados\OAMBundle\Model\Results\CategoryType\CategoryType;
use AsResultados\OAMBundle\Model\Results\Match\Embed\Card;
use AsResultados\OAMBundle\Model\Results\Match\Embed\Goal;
use AsResultados\OAMBundle\Model\Results\Match\Embed\Goals;
use AsResultados\OAMBundle\Model\Results\Match\Embed\PenaltiesShootOut;
use AsResultados\OAMBundle\Model\Results\Match\Embed\PenaltyShootOut;
use AsResultados\OAMBundle\Model\Results\Match\Embed\PersonRol;
use AsResultados\OAMBundle\Model\Results\Match\Embed\Substitution;
use AsResultados\OAMBundle\Model\Results\Match\Embed\SubstitutionPlayer;
use AsResultados\OAMBundle\Model\Results\Match\Embed\Summoned;
use AsResultados\OAMBundle\Model\Results\Match\Embed\Team;
use AsResultados\OAMBundle\Model\Results\Match\Match;
use AsResultados\OAMBundle\Model\Results\MatchPersonStat\MatchPersonStat;
use AsResultados\OAMBundle\Model\Results\MatchTeamStat\Embed\Period;
use AsResultados\OAMBundle\Model\Results\MatchTeamStat\Embed\Stat as StatTeam;
use AsResultados\OAMBundle\Model\Results\MatchPersonStat\Embed\Stat as StatPerson;
use AsResultados\OAMBundle\Model\Results\MatchTeamStat\MatchTeamStat;
use AsResultados\OAMBundle\Model\Collection\ProviderIdsCollection;
use AsResultados\OAMBundle\Exception\EmptyItemException;
use AsResultados\OAMBundle\Exception\MappingException;
use App\Processor\Provider\Traits\CompetitionSeasonIdTrait;
use App\Processor\Provider\Traits\MatchIdTrait;
use App\Utils\Date;
use App\Utils\FootballTrait;
use AsResultados\OAMBundle\Model\Results\Moment;
use DateTimeZone;
use Exception;
use Symfony\Component\DomCrawler\Crawler;

class F9 extends AbstractProcessor
{
    use FootballTrait;
    use MatchIdTrait;
    use CompetitionSeasonIdTrait;

    /**
     * @var string
     */
    protected $matchProviderId;

    /**
     * @inheritDoc
     */
    protected function setProcessorVariablesFromMapping(): void
    {
        $mappingCollection = MappingCollection::getInstance();
        $this->setCompetitionSeasonId($mappingCollection->get(
            MappingInterface::ENTITY_COMPETITION_SEASON, self::PROVIDER, $this->getCompetitionSeasonFromXml()
        ));
        $this->setMatchId($mappingCollection->get(
            MappingInterface::ENTITY_MATCH, self::PROVIDER, $this->getMatchIdFromXml()
        ));
        $this->setMappingsOwn($this->getMappingsOwn());
    }

    /**
     * @inheritDoc
     */
    public function run(): bool
    {
        $match = $this->getMatchFromXml();
        $matchRegister = MatchRegister::getInstance($this->getClient());
        //Update match
        try {
            //Patch instead of put because we do not change structure data (matchday, groupnumber, stage ... etc).
            $matchRegister->patch([$match]);
            $opUpdateMatch = true;
        } catch (EmptyItemException $e) {
            $this->getLogger()->info('Can not update match: ' . $e->getMessage());
        } catch (Exception $e) {
            $this->getLogger()->error('Can not update match: ' . $e->getMessage());
        }
        $statsTeams = $this->getStatsTeamsFromXmlNode();
        $matchTeamStatRegister = MatchTeamStatRegister::getInstance($this->getClient());
        //Update matchStatsTeams
        try {
            $matchTeamStatRegister->patch($statsTeams->getAllRegistered());
            $opUpdateMatchTeamsStats = true;
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not update matchTeamsStats: ' . $e->getMessage());
        }
        //Insert matchStatsTeams
        try {
            $matchTeamStatRegister->post($statsTeams->getAllUnRegistered());
            $opInsertMatchTeamsStats = true;
            //Add new matchStatsTeams to collection
            $statsTeams->removeAllUnRegistered();
            $statsTeams->addMultipleRegistered($matchTeamStatRegister->getLastInsertedItems());
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not insert matchTeamsStats: ' . $e->getMessage());
        }
        $statsPeople = $this->getStatsPlayersFromXmlNode();
        $matchPersonStatRegister = MatchPersonStatRegister::getInstance($this->getClient());
        //Update matchStatsPeople
        try {
            $matchPersonStatRegister->patch($statsPeople->getAllRegistered());
            $opUpdateMatchPeopleStats = true;
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not update matchPeopleStats: ' . $e->getMessage());
        }
        //Insert matchStatsPeople
        try {
            $matchPersonStatRegister->post($statsPeople->getAllUnRegistered());
            $opInsertMatchPeopleStats = true;
            //Add new matchStatsPeople to collection
            $statsPeople->removeAllUnRegistered();
            $statsPeople->addMultipleRegistered($matchPersonStatRegister->getLastInsertedItems());
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not insert matchPeopleStats: ' . $e->getMessage());
        }
        //Dispatch events
        if (isset($opUpdateMatch) || isset($opUpdateMatchTeamsStats) || isset($opInsertMatchTeamsStats) ||
            isset($opUpdateMatchPeopleStats) || isset($opInsertMatchPeopleStats)) {
            $this->dispatchEvents($match);
        }
        return true;
    }

    /**
     * @param Match $match
     */
    private function dispatchEvents(Match $match): void
    {
        $event = new LiveMatchEvent();
        $event->setMatch($match->getId());
        $this->dispatcher->dispatch($event, $event->getEventName());
    }

    /**
     * @inheritDoc
     */
    protected function getMappingsFromProviderData(): array
    {
        $mappings = array();
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_COMPETITION_SEASON, self::PROVIDER);
        $collection->addId($this->getCompetitionSeasonFromXml());
        $mappings[] = $collection;
        $mappings[] = $this->getMappingTeamsFromXml();
        $mappings[] = $this->getMappingPeopleFromXml();
        $mappings[] = $this->getMappingVenuesFromXml();
        $mappings[] = $this->getMappingMatchesFromXml();
        $mappings[] = new ProviderIdsCollection(MappingInterface::ENTITY_ROL, self::PROVIDER, true);
        $mappings[] = new ProviderIdsCollection(MappingInterface::ENTITY_MATCH_PERIOD, self::PROVIDER, true);
        $mappings[] = new ProviderIdsCollection(MappingInterface::ENTITY_TEAM_FORMATION, self::PROVIDER, true);
        $mappings[] = new ProviderIdsCollection(MappingInterface::ENTITY_PENALTY_SHOOT, self::PROVIDER, true);
        $mappings[] = new ProviderIdsCollection(MappingInterface::ENTITY_SUBSTITUTION_REASON, self::PROVIDER, true);
        $mappings[] = new ProviderIdsCollection(MappingInterface::ENTITY_BOOKING_CARD, self::PROVIDER, true);
        $mappings[] = new ProviderIdsCollection(MappingInterface::ENTITY_STAT_PLAYER, self::PROVIDER, true);
        $mappings[] = new ProviderIdsCollection(MappingInterface::ENTITY_STAT_TEAM, self::PROVIDER, true);
        return $mappings;
    }

    /**
     * @throws Exception
     */
    protected function getMappingsOwn(): array
    {
        $requests = array();
        $request = new MatchTeamStatRequest();
        $requests[] = new RequestsResourceItem(
            [$request->getByMatchFieldsOnlyIdsAndTeam($this->getMatchId())],
            $request->getResource()
        );
        $request = new MatchPersonStatRequest();
        $requests[] = new RequestsResourceItem(
            [$request->getByMatchFieldsOnlyIdsAndPerson($this->getMatchId())],
            $request->getResource()
        );
        return $requests;
    }

    /**
     * @return Match
     * @throws Exception
     */
    protected function getMatchFromXml(): Match
    {
        $data = $this->getCrawledXmlDocument()->filterXPath('SoccerFeed/SoccerDocument[1]/@uID');
        if ($data->count() !== 1) {
            throw new Exception('Match has no id');
        }
        $mappingCollection = MappingCollection::getInstance();
        $match = new Match();
        //If no mapping for match, we can not process the match
        //For unknown reason opta changes the id in the match and change the g for an f
        $idMatch = $data->text();
        $idMatch = str_replace('f', 'g', $idMatch);
        if (!$mappingCollection->exists($mappingCollection::ENTITY_MATCH, self::PROVIDER, $idMatch)) {
            throw new MappingException(self::PROVIDER, $idMatch, $mappingCollection::ENTITY_MATCH);
        }
        $match->setId($mappingCollection->get($mappingCollection::ENTITY_MATCH, self::PROVIDER, $idMatch));
        //Detail
        $match->setDetailById(CategoryInterface::MATCH_DETAIL_ALL_STATS);
        //Date
        $data = $this->getCrawledXmlDocument()->filterXPath('SoccerFeed/SoccerDocument[1]/MatchData/MatchInfo/Date');
        if ($data->count() > 0) {
            try {
                $date = new Date($data->text(), new DateTimeZone(self::DATETIME_ZONE));
                $match->setDate($date->getTimestamp());
                $match->setDateConfirmed(true);
            } catch (Exception $e) {
                //Do nothing, date will not be set
            }
        }
        //State
        $match->setStateById($this->getCategoryStateFromXml()->getId());
        //Moment
        $data = $this->getCrawledXmlDocument()->filterXPath('SoccerFeed/SoccerDocument[1]/MatchData/MatchInfo/@Period');
        if ($data->count() > 0) {
            if ($mappingCollection->exists(
                $mappingCollection::ENTITY_MATCH_PERIOD,
                self::PROVIDER,
                $data->text())) {
                $time = $this->getCrawledXmlDocument()->filterXPath(
                    'SoccerFeed/SoccerDocument[1]/MatchData/Stat[@Type="match_time"]'
                );
                if ($time->count() > 0) {
                    $period = $mappingCollection->get(
                        $mappingCollection::ENTITY_MATCH_PERIOD,
                        self::PROVIDER,
                        $data->text()
                    );
                    $moment = new Moment();
                    $moment->setPeriodById($period);
                    $category = new Category();
                    $category->setId($period);
                    $moment->setElapsedSeconds($this->getRelativeSecondsForPeriod(((int)$time->text() * 60), $category));
                    $match->setMoment($moment);
                }
            }
        }
        //Periods
        $data = $this->getPeriodsFromXml();
        if (!empty($data)) {
            $match->setPeriods($data);
        }
        //Leg
        $data = $this->getCrawledXmlDocument()->filterXPath('SoccerFeed/SoccerDocument[1]/MatchData/MatchInfo/@MatchType');
        if ($data->count() > 0) {
            try {
                $match->setLeg($this->generateLegNumber($data->text()));
            } catch (Exception $e) {
                //Do nothing, leg will not be set
            }
        }
        //PreviousMatch
        $data = $this->getCrawledXmlDocument()->filterXPath('SoccerFeed/SoccerDocument[1]/MatchData/PreviousMatch/@MatchRef');
        //For unknown reason opta changes the id in the match and change the g for an f
        if ($data->count() > 0) {
            $match->setPreviousMatchById($mappingCollection->get(
                $mappingCollection::ENTITY_MATCH,
                self::PROVIDER,
                str_replace('f', 'g', $data->text())
            ));
        }
        //Winner/Classified
        $data = $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument[1]/MatchData/MatchInfo/Result/@Winner');
        if ($data->count() > 0) {
            if ($mappingCollection->exists(
                $mappingCollection::ENTITY_TEAM,
                self::PROVIDER,
                $data->text()
            )) {
                //Opta sends here the winner or the classified, so we have to take it into account
                if (is_null($match->getPreviousMatch())) {
                    $match->setWinnerById($mappingCollection->get(
                        $mappingCollection::ENTITY_TEAM,
                        self::PROVIDER,
                        $data->text()
                    ));
                } else {
                    $match->setClassifiedById($mappingCollection->get(
                        $mappingCollection::ENTITY_TEAM,
                        self::PROVIDER,
                        $data->text()
                    ));
                }
            }
        }
        //Teams
        $teamXml = $this->getCrawledXmlDocument()->filterXPath('SoccerFeed/SoccerDocument[1]/MatchData/TeamData[@Side="Home"]');
        if ($teamXml->count() > 0) {
            $match->setLocal($this->createMatchTeamFromXmlNode($teamXml));
        }
        $teamXml = $this->getCrawledXmlDocument()->filterXPath('SoccerFeed/SoccerDocument[1]/MatchData/TeamData[@Side="Away"]');
        if ($teamXml->count() > 0) {
            $match->setVisitor($this->createMatchTeamFromXmlNode($teamXml));
        }
        //Venue
        $data = $this->getCrawledXmlDocument()->filterXPath('SoccerFeed/SoccerDocument[1]/Venue/@uID');
        if ($data->count() > 0) {
            //For unknown reason opta add a v before the id in the f9 (it is not in the F40)
            $id = filter_var($data->text(), FILTER_SANITIZE_NUMBER_INT);
            if ($mappingCollection->exists($mappingCollection::ENTITY_VENUE, self::PROVIDER, $id)) {
                $match->setVenueById($mappingCollection->get($mappingCollection::ENTITY_VENUE, self::PROVIDER, $id));
            }
        }
        //Attendance
        $data = $this->getCrawledXmlDocument()->filterXPath('SoccerFeed/SoccerDocument[1]/MatchData/MatchInfo/Attendance');
        if ($data->count() > 0) {
            $match->setAttendance($data->text());
        }
        //Referees
        $referees = $this->getRefereesFromXml();
        if (!empty($referees)) {
            $match->setReferees($referees);
        }
        return $match;
    }

    /**
     * @return Category
     */
    protected function getCategoryStateFromXml(): Category
    {
        $category = new Category();
        $categoryType = new CategoryType();
        $categoryType->setId($categoryType::TYPE_MATCH_STATE);
        $category->setType($categoryType);
        //Finished
        $data = $this->getCrawledXmlDocument()->filterXPath('SoccerFeed/SoccerDocument[1][@Type="Result"]');
        if ($data->count() > 0) {
            $category->setId($category::MATCH_STATE_FINISHED);
            return $category;
        }
        //Postponed and Abandoned
        $data = $this->getCrawledXmlDocument()->filterXPath('SoccerFeed/SoccerDocument[1]/MatchData/MatchInfo/Result/@Type');
        if ($data->count() > 0) {
            $data = strtolower($data->text());
            if ($data === 'postponed') {
                $category->setId($category::MATCH_STATE_POSTPONED);
                return $category;
            } else if ($data === 'abandoned') {
                $category->setId($category::MATCH_STATE_ABANDONED);
                return $category;
            }
        }
        //Pre-match
        $data = $this->getCrawledXmlDocument()->filterXPath('SoccerFeed/SoccerDocument[1]/MatchData/MatchInfo/@Period');
        if ($data->count() > 0) {
            $data = strtolower($data->text());
            if ($data === 'prematch') {
                $category->setId($category::MATCH_STATE_PRE_MATCH);
                return $category;
            }
        }
        //Live
        $category->setId($category::MATCH_STATE_LIVE);
        return $category;
    }

    /**
     * @return Moment[]
     * @throws Exception
     */
    protected function getPeriodsFromXml(): array
    {
        $periods = array();
        $searches = array(
            'first_half',
            'second_half',
            'first_half_extra',
            'second_half_extra'
        );
        $mappingCollection = MappingCollection::getInstance();
        foreach ($searches as $search) {
            if (!$mappingCollection->exists($mappingCollection::ENTITY_MATCH_PERIOD, self::PROVIDER, $search)) {
                continue;
            }
            $time = $this->getCrawledXmlDocument()->filterXPath(
                'SoccerFeed/SoccerDocument[1]/MatchData/Stat[@Type="' . $search . '_time"]'
            );
            if ($time->count() > 0) {
                $period = $mappingCollection->get($mappingCollection::ENTITY_MATCH_PERIOD, self::PROVIDER, $search);
                $moment = new Moment();
                $moment->setPeriodById($period);
                $moment->setElapsedSeconds((int)$time->text() * 60);
                $periods[] = $moment;
            }
        }
        return $periods;
    }

    /**
     * @return PersonRol[]
     * @throws Exception
     */
    protected function getRefereesFromXml(): array
    {
        $referees = array();
        $refereesSearch = array();
        $mappingCollection = MappingCollection::getInstance();
        //Main referees
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument[1]/MatchData/MatchOfficial')
            ->each(function (Crawler $node) use (&$refereesSearch) {
                $data = $node->filterXPath('MatchOfficial/OfficialData/OfficialRef/@Type');
                if ($data->count() > 0) {
                    $refereesSearch[$node->attr('uID')] = $data->text();
                }
            });
        //Extra referees
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument[1]/MatchData/AssistantOfficials/AssistantOfficial')
            ->each(function (Crawler $node) use (&$refereesSearch) {
                $refereesSearch[$node->attr('uID')] = $node->attr('Type');
            });
        //Create our own data model
        foreach ($refereesSearch as $id => $type) {
            if (!$mappingCollection->exists($mappingCollection::ENTITY_PERSON, self::PROVIDER, $id)) {
                continue;
            }
            if (!$mappingCollection->exists($mappingCollection::ENTITY_ROL, self::PROVIDER, $type)) {
                $this->getLogger()->warning('Rol for referees does not exist: ' . $type);
                continue;
            }
            $personRol = new PersonRol();
            $personRol->setPersonById($mappingCollection->get($mappingCollection::ENTITY_PERSON, self::PROVIDER, $id));
            $personRol->setRolById($mappingCollection->get($mappingCollection::ENTITY_ROL, self::PROVIDER, $type));
            $referees[] = $personRol;
        }
        return $referees;
    }

    /**
     * @param Crawler $node
     * @return Team
     * @throws MappingException
     * @throws Exception
     */
    protected function createMatchTeamFromXmlNode(Crawler $node): Team
    {
        $mappingCollection = MappingCollection::getInstance();
        $team = new Team();
        //Team id
        $team->setTeamById($mappingCollection->get(
            $mappingCollection::ENTITY_TEAM,
            self::PROVIDER,
            $node->attr('TeamRef'))
        );
        //Goals
        try {
            $data = $this->createMatchTeamGoalsFromXmlNode($node);
            $team->setGoals($data);
        } catch (Exception $e) {
            $this->getLogger()->warning('Can not insert goals: ' . $e->getMessage());
        }
        //Cards
        try {
            $data = $this->createMatchTeamCardsFromXmlNode($node);
            //We add this always although it may be empty because maybe there has been an error and we have to eliminate
            $team->setCards($data);
        } catch (Exception $e) {
            $this->getLogger()->warning('Can not insert cards: ' . $e->getMessage());
        }
        //Penalties
        try {
            $data = $this->createMatchTeamPenaltiesFromXmlNode($node);
            $team->setPenaltiesShoutOut($data);
        } catch (Exception $e) {
            $this->getLogger()->warning('Can not insert penalties: ' . $e->getMessage());
        }
        //Substitutions
        try {
            $data = $this->createMatchTeamSubstitutionsFromXmlNode($node);
            //We add this always although it may be empty because maybe there has been an error and we have to eliminate
            $team->setSubstitutions($data);
        } catch (Exception $e) {
            $this->getLogger()->warning('Can not insert substitutions: ' . $e->getMessage());
        }
        //Formation used
        $data = $node->filterXPath('TeamData/Stat[@Type="formation_used"]');
        if ($data->count() > 0 && !empty($data->text())) {
            if ($mappingCollection->exists(
                $mappingCollection::ENTITY_TEAM_FORMATION,
                self::PROVIDER,
                $data->text()
            )) {
                $team->setFormationById($mappingCollection->get(
                    $mappingCollection::ENTITY_TEAM_FORMATION,
                    self::PROVIDER,
                    $data->text()
                ));
            } else {
                $this->getLogger()->notice('Team formation without mapping: ' . $data->text());
            }
        }
        //Summoned
        try {
            $data = $this->createMatchTeamSummonedFromXmlNode($node, $team->getFormation());
            if (!empty($data)) {
                $team->setSummoned($data);
            }
        } catch (Exception $e) {
            $this->getLogger()->warning('Can not insert summoned: ' . $e->getMessage());
        }
        //Managers
        try {
            $team->setManagers($this->createMatchTeamManagersFromXmlNode($node));
        } catch (Exception $e) {
            $this->getLogger()->warning('Can not insert managers: ' . $e->getMessage());
        }
        return $team;
    }

    /**
     * @param Crawler $node
     * @return Goals
     */
    protected function createMatchTeamGoalsFromXmlNode(Crawler $node): Goals
    {
        $mappingCollection = MappingCollection::getInstance();
        $goals = new Goals();
        $goals->setScore($node->attr('Score'));
        $goalsDetails = array();
        $node->filterXPath('TeamData/Goal')
            ->each(function (Crawler $dataXml) use ($mappingCollection, &$goalsDetails) {
                try {
                    $goal = new Goal();
                    //Person
                    $goal->setPlayerById($mappingCollection->get(
                        $mappingCollection::ENTITY_PERSON,
                        self::PROVIDER,
                        $dataXml->attr('PlayerRef'))
                    );
                    $this->setGoalType($goal, $dataXml->attr('Type'));
                    //Period
                    if ($mappingCollection->exists(
                        $mappingCollection::ENTITY_MATCH_PERIOD,
                        self::PROVIDER,
                        $dataXml->attr('Period')
                    )) {
                        $period = $mappingCollection->get(
                            $mappingCollection::ENTITY_MATCH_PERIOD,
                            self::PROVIDER,
                            $dataXml->attr('Period')
                        );
                        $moment = new Moment();
                        $moment->setPeriodById($period);
                        $categoryPeriod = new Category();
                        $categoryPeriod->setId($period);
                        $moment->setElapsedSeconds($this->getRelativeSecondsForPeriod(
                            $dataXml->attr('Min') * 60 + $dataXml->attr('Sec'),
                            $categoryPeriod)
                        );
                        $goal->setMoment($moment);
                    }
                    $goalsDetails[] = $goal;
                } catch (MappingException $e) {
                    //Keep going, we won't insert that goal
                    return;
                }
            });
        //We add this always although it may be empty because maybe there has been an error and we have to eliminate
        $goals->setDetails($goalsDetails);
        return $goals;
    }

    /**
     * @param Crawler $node
     * @return Card[]
     */
    protected function createMatchTeamCardsFromXmlNode(Crawler $node): array
    {
        $mappingCollection = MappingCollection::getInstance();
        $cards = array();
        $node->filterXPath('TeamData/Booking')
            ->each(function (Crawler $dataXml) use ($mappingCollection, &$cards) {
                try {
                    $card = new Card();
                    //Person
                    $card->setPlayerById($mappingCollection->get(
                        $mappingCollection::ENTITY_PERSON,
                        self::PROVIDER,
                        $dataXml->attr('PlayerRef'))
                    );
                    //Card Type
                    $card->setTypeById($mappingCollection->get(
                        $mappingCollection::ENTITY_BOOKING_CARD,
                        self::PROVIDER,
                        $dataXml->attr('CardType'))
                    );
                    //Period
                    if ($mappingCollection->exists(
                        $mappingCollection::ENTITY_MATCH_PERIOD,
                        self::PROVIDER,
                        $dataXml->attr('Period')
                    )) {
                        $period = $mappingCollection->get(
                            $mappingCollection::ENTITY_MATCH_PERIOD,
                            self::PROVIDER,
                            $dataXml->attr('Period')
                        );
                        $moment = new Moment();
                        $moment->setPeriodById($period);
                        $categoryPeriod = new Category();
                        $categoryPeriod->setId($period);
                        $moment->setElapsedSeconds($this->getRelativeSecondsForPeriod(
                            $dataXml->attr('Min') * 60 + $dataXml->attr('Sec'),
                            $categoryPeriod)
                        );
                        $card->setMoment($moment);
                    }
                    $cards[] = $card;
                } catch (MappingException $e) {
                    //Keep going, we won't insert that card
                    return;
                }
            });
        return $cards;
    }

    /**
     * @param Crawler $node
     * @return Substitution[]
     */
    protected function createMatchTeamSubstitutionsFromXmlNode(Crawler $node): array
    {
        $mappingCollection = MappingCollection::getInstance();
        $substitutions = array();
        $node->filterXPath('TeamData/Substitution')
            ->each(function (Crawler $dataXml) use ($mappingCollection, &$substitutions) {
                try {
                    $substitution = new Substitution();
                    $substitutionPlayers = new SubstitutionPlayer();
                    //Person in
                    if (!is_null($dataXml->attr('SubOn'))) {
                        $substitutionPlayers->setInById($mappingCollection->get(
                            $mappingCollection::ENTITY_PERSON,
                            self::PROVIDER,
                            $dataXml->attr('SubOn'))
                        );
                    }
                    //Person out
                    $substitutionPlayers->setOutById($mappingCollection->get(
                        $mappingCollection::ENTITY_PERSON,
                        self::PROVIDER,
                        $dataXml->attr('SubOff'))
                    );
                    $substitution->setPlayers($substitutionPlayers);
                    //Reason
                    if ($dataXml->attr('Retired') === '1') {
                        $substitution->setReasonById($mappingCollection->get(
                            $mappingCollection::ENTITY_SUBSTITUTION_REASON,
                            self::PROVIDER,
                            'Retired')
                        );
                    } else {
                        $substitution->setReasonById($mappingCollection->get(
                            $mappingCollection::ENTITY_SUBSTITUTION_REASON,
                            self::PROVIDER,
                            $dataXml->attr('Reason'))
                        );
                    }
                    //Period
                    if ($mappingCollection->exists(
                        $mappingCollection::ENTITY_MATCH_PERIOD,
                        self::PROVIDER,
                        $dataXml->attr('Period')
                    )) {
                        $period = $mappingCollection->get(
                            $mappingCollection::ENTITY_MATCH_PERIOD,
                            self::PROVIDER,
                            $dataXml->attr('Period')
                        );
                        $moment = new Moment();
                        $moment->setPeriodById($period);
                        $categoryPeriod = new Category();
                        $categoryPeriod->setId($period);
                        $moment->setElapsedSeconds($this->getRelativeSecondsForPeriod(
                            $dataXml->attr('Min') * 60 + $dataXml->attr('Sec'),
                            $categoryPeriod)
                        );
                        $substitution->setMoment($moment);
                    }
                    $substitutions[] = $substitution;
                } catch (MappingException $e) {
                    //Keep going, we won't insert that substitution
                    return;
                }
            });
        return $substitutions;
    }

    /**
     * @param Crawler $node
     * @param Category|null $teamFormation
     * @return Summoned[]
     */
    protected function createMatchTeamSummonedFromXmlNode(Crawler $node, ?Category $teamFormation): array
    {
        $mappingCollection = MappingCollection::getInstance();
        $summoneds = array();
        $positionError = false;
        $node->filterXPath('TeamData/PlayerLineUp/MatchPlayer')
            ->each(function (Crawler $dataXml) use ($mappingCollection, &$summoneds, $teamFormation, &$positionError) {
                try {
                    $summoned = new Summoned();
                    //Person
                    $summoned->setPlayerById($mappingCollection->get(
                        $mappingCollection::ENTITY_PERSON,
                        self::PROVIDER,
                        $dataXml->attr('PlayerRef'))
                    );
                    //Substitute
                    $position = $dataXml->attr('Position');
                    if ($position === 'Substitute') {
                        $summoned->setSubstitute(true);
                        //If position === substitute then the real position is in the subposition attribute (only in this case)
                        $position = $dataXml->attr('SubPosition');
                    } else {
                        $summoned->setSubstitute(false);
                    }
                    //Rol
                    $summoned->setRolById($mappingCollection->get(
                        $mappingCollection::ENTITY_ROL,
                        self::PROVIDER,
                        $position)
                    );
                    //Position
                    if (!is_null($teamFormation)) {
                        $data = $dataXml->filterXPath('MatchPlayer/Stat[@Type="formation_place"]');
                        if ($data->count() > 0) {
                            try {
                                $summoned->setPosition($this->getFormationPlace((int)$data->text(), $teamFormation));
                            } catch (MissingItemException $e) {
                                //Track position error and keep going
                                $positionError = true;
                            } catch (Exception $e) {
                                //Do nothing and keep going
                            }
                        }
                    }
                    $summoneds[] = $summoned;
                } catch (MappingException $e) {
                    //Keep going, we won't insert that summoned
                    return;
                }
            });
        if ($positionError === true) {
            $this->getLogger()->notice('Position is not being set because we do not have that provider formation in batch: ' . $teamFormation->getId());
        }
        return $summoneds;
    }

    /**
     * @param Crawler $node
     * @return PersonRol[]
     */
    protected function createMatchTeamManagersFromXmlNode(Crawler $node): array
    {
        $mappingCollection = MappingCollection::getInstance();
        $id = $node->attr('TeamRef');
        $managers = array();
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument[1]/Team[@uID="' . $id . '"]/TeamOfficial')
            ->each(function (Crawler $dataXml) use ($mappingCollection, &$managers) {
                try {
                    $personRol = new PersonRol();
                    //Person
                    $personRol->setPersonById($mappingCollection->get(
                        $mappingCollection::ENTITY_PERSON,
                        self::PROVIDER,
                        $dataXml->attr('uID')
                    ));
                    //Rol
                    $personRol->setRolById($mappingCollection->get(
                        $mappingCollection::ENTITY_ROL,
                        self::PROVIDER,
                        $dataXml->attr('Type')
                    ));
                    $managers[] = $personRol;
                } catch (MappingException $e) {
                    //Keep going, we won't insert that manager
                    return;
                }
            });
        return $managers;
    }

    /**
     * @param Crawler $node
     * @return PenaltiesShootOut
     */
    protected function createMatchTeamPenaltiesFromXmlNode(Crawler $node): PenaltiesShootOut
    {
        $mappingCollection = MappingCollection::getInstance();
        $penalties = new PenaltiesShootOut();
        $penalties->setScore($node->attr('ShootOutScore'));
        $penaltiesDetails = array();
        $node->filterXPath('TeamData/ShootOut/PenaltyShot')
            ->each(function (Crawler $dataXml) use ($mappingCollection, &$penaltiesDetails) {
                try {
                    $penalty = new PenaltyShootOut();
                    //Person
                    $penalty->setPlayerById($mappingCollection->get(
                        $mappingCollection::ENTITY_PERSON,
                        self::PROVIDER,
                        $dataXml->attr('PlayerRef')
                    ));
                    //Shoot
                    $penalty->setShootById($mappingCollection->get(
                        $mappingCollection::ENTITY_PENALTY_SHOOT,
                        self::PROVIDER,
                        $dataXml->attr('Outcome')
                    ));
                    //Period
                    $categoryPeriod = new Category();
                    $moment = new Moment();
                    $moment->setPeriodById($categoryPeriod::MATCH_PERIOD_SHOOT_OUT);
                    $moment->setElapsedSeconds($this->getRelativeSecondsForPeriod(
                        $dataXml->attr('Min') * 60 + $dataXml->attr('Sec'),
                        $categoryPeriod
                    ));
                    $penalty->setMoment($moment);
                    $penaltiesDetails[] = $penalty;
                } catch (MappingException $e) {
                    //Keep going, we won't insert that penalty
                    return;
                }
            });
        //We add this always although it may be empty because maybe there has been an error and we have to eliminate
        $penalties->setDetails($penaltiesDetails);
        return $penalties;
    }

    /**
     * @return Collection
     */
    protected function getStatsTeamsFromXmlNode(): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $collection = new Collection(MatchTeamStat::class);
        $this->getCrawledXmlDocument()->filterXPath('SoccerFeed/SoccerDocument[1]/MatchData/TeamData')
            ->each(function (Crawler $dataXml) use ($mappingCollection, &$collection) {
                try {
                    $stats = array();
                    $dataXml->filterXPath('TeamData/Stat')->each(function (Crawler $dataXml) use (&$stats, $mappingCollection) {
                        if (!$mappingCollection->exists(
                            $mappingCollection::ENTITY_STAT_TEAM,
                            self::PROVIDER,
                            $dataXml->attr('Type')
                        )) {
                            //No mapping for the stat, keep going
                            return;
                        }
                        $stat = new StatTeam();
                        //Type
                        $stat->setStatById($mappingCollection->get(
                            $mappingCollection::ENTITY_STAT_TEAM,
                            self::PROVIDER,
                            $dataXml->attr('Type')
                        ));
                        //Value
                        $stat->setValue($dataXml->text());
                        //Periods
                        $periodsXml = array('FH', 'SH', 'EFH', 'ESH');
                        $periods = array();
                        foreach ($periodsXml as $periodXml) {
                            if (!is_null($dataXml->attr($periodXml))) {
                                $period = new Period();
                                $period->setPeriodById($mappingCollection->get(
                                    $mappingCollection::ENTITY_MATCH_PERIOD,
                                    self::PROVIDER,
                                    $periodXml
                                ));
                                $period->setValue($dataXml->attr($periodXml));
                                $periods[] = $period;
                            }
                        }
                        $stat->setPeriods($periods);
                        $stats[] = $stat;
                    });
                    if (empty($stats)) {
                        //No stats for this team, keep going
                        return;
                    }
                    $obj = new MatchTeamStat();
                    $obj->setTeamById($mappingCollection->get(
                        $mappingCollection::ENTITY_TEAM,
                        self::PROVIDER,
                        $dataXml->attr('TeamRef'))
                    );
                    $obj->setMatchById($this->matchId);
                    $obj->setCompetitionSeasonById($this->getCompetitionSeasonId());
                    $obj->setStats($stats);
                    if ($mappingCollection->existsOwn(MatchTeamStat::class, $obj->getUniqueIdByRelations())) {
                        $obj->setId($mappingCollection->getOwn(MatchTeamStat::class, $obj->getUniqueIdByRelations()));
                        $collection->addRegistered($obj);
                    } else {
                        $collection->addUnRegistered($obj, $obj->getUniqueIdByRelations());
                    }
                } catch (MappingException $e) {
                    //Keep going, we won't insert stats for the team
                    return;
                }
            });
        return $collection;
    }

    /**
     * @return Collection
     */
    protected function getStatsPlayersFromXmlNode(): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $collection = new Collection(MatchPersonStat::class);
        $this->getCrawledXmlDocument()->filterXPath('SoccerFeed/SoccerDocument[1]/MatchData/TeamData/PlayerLineUp/MatchPlayer')
            ->each(function (Crawler $dataXml) use ($mappingCollection, $collection) {
                try {
                    $stats = array();
                    $dataXml->filterXPath('MatchPlayer/Stat')->each(function (Crawler $dataXml) use (&$stats, $mappingCollection) {
                        if (!$mappingCollection->exists(
                            $mappingCollection::ENTITY_STAT_PLAYER,
                            self::PROVIDER,
                            $dataXml->attr('Type')
                        )) {
                            //No mapping for the stat, keep going
                            return;
                        }
                        $stat = new StatPerson();
                        //Type
                        $stat->setStatById($mappingCollection->get(
                            $mappingCollection::ENTITY_STAT_PLAYER,
                            self::PROVIDER,
                            $dataXml->attr('Type')
                        ));
                        //Value
                        $stat->setValue($dataXml->text());
                        $stats[] = $stat;
                    });
                    if (empty($stats)) {
                        //No stats for this player, keep going
                        return;
                    }
                    $obj = new MatchPersonStat();
                    $obj->setPersonById($mappingCollection->get(
                        $mappingCollection::ENTITY_PERSON,
                        self::PROVIDER,
                        $dataXml->attr('PlayerRef')
                    ));
                    $obj->setMatchById($this->matchId);
                    $obj->setTeamById($mappingCollection->get(
                        $mappingCollection::ENTITY_TEAM,
                        self::PROVIDER,
                        $dataXml->parents()->parents()->attr('TeamRef')
                    ));
                    $obj->setCompetitionSeasonById($this->getCompetitionSeasonId());
                    $obj->setStats($stats);
                    if ($mappingCollection->existsOwn(MatchPersonStat::class, $obj->getUniqueIdByRelations())) {
                        $obj->setId($mappingCollection->getOwn(MatchPersonStat::class, $obj->getUniqueIdByRelations()));
                        $collection->addRegistered($obj);
                    } else {
                        $collection->addUnRegistered($obj, $obj->getUniqueIdByRelations());
                    }
                } catch (MappingException $e) {
                    //Keep going, we won't insert stats for the person
                    return;
                }
            });
        return $collection;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingMatchesFromXml(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_MATCH, self::PROVIDER);
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument[@uID]')
            ->each(function (Crawler $node) use ($collection) {
                //For unknown reason opta changes the id in the match and change the g for an f
                $id = $node->attr('uID');
                $id = str_replace('f', 'g', $id);
                $collection->addId($id);
            });
        return $collection;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingTeamsFromXml(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_TEAM, self::PROVIDER);
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument[1]/MatchData/TeamData[@TeamRef]')
            ->each(function (Crawler $node) use ($collection) {
                $collection->addId($node->attr('TeamRef'));
            });
        return $collection;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingPeopleFromXml(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_PERSON, self::PROVIDER);
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument[1]/MatchData/MatchOfficial[@uID]')
            ->each(function (Crawler $node) use ($collection) {
                $collection->addId($node->attr('uID'));
            });
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument[1]/MatchData/AssistantOfficials/AssistantOfficial[@uID]')
            ->each(function (Crawler $node) use ($collection) {
                $collection->addId($node->attr('uID'));
            });
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument[1]/Team/Player[@uID]')
            ->each(function (Crawler $node) use ($collection) {
                $collection->addId($node->attr('uID'));
            });
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument[1]/Team/TeamOfficial[@uID]')
            ->each(function (Crawler $node) use ($collection) {
                $collection->addId($node->attr('uID'));
            });
        return $collection;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingVenuesFromXml(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_VENUE, self::PROVIDER);
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument[1]/Venue[@uID]')
            ->each(function (Crawler $node) use ($collection) {
                //For unknown reason opta add a v before the id in the f9 (it is not in the F40)
                $id = filter_var($node->attr('uID'), FILTER_SANITIZE_NUMBER_INT);
                $collection->addId($id);
            });
        return $collection;
    }

    /**
     * @return string
     * @throws MissingItemException
     */
    protected function getCompetitionFromXml(): string
    {
        if (!isset($this->competitionProviderId)) {
            $node = $this->getCrawledXmlDocument()->filterXPath('SoccerFeed/SoccerDocument[1]/Competition');
            if (is_null($node->attr('uID'))) {
                throw new MissingItemException('uID for competition id', 'xml node', 'provider xml file');
            }
            //Only in this case opta will send the id with a c letter in the beginning
            $this->competitionProviderId = substr((string)$node->attr('uID'), 1);
        }
        return $this->competitionProviderId;
    }

    /**
     * @return string
     * @throws MissingItemException
     */
    protected function getSeasonFromXml(): string
    {
        if (!isset($this->seasonProviderId)) {
            $node = $this->getCrawledXmlDocument()->filterXPath('SoccerFeed/SoccerDocument[1]/Competition/Stat[@Type="season_id"]');
            if ($node->count() !== 1) {
                throw new MissingItemException('node with season id id', 'xml node', 'provider xml file');
            }
            $this->seasonProviderId = (string)$node->text();
        }
        return $this->seasonProviderId;
    }

    /**
     * @return string
     * @throws MissingItemException
     */
    protected function getMatchIdFromXml(): string
    {
        if (!isset($this->matchProviderId)) {
            $node = $this->getCrawledXmlDocument()->filterXPath('SoccerFeed/SoccerDocument[1]');
            if (is_null($node->attr('uID'))) {
                throw new MissingItemException('uID for match id', 'xml node', 'provider xml file');
            }
            //For unknown reason opta changes the id in the match and change the g for an f
            $this->matchProviderId = str_replace('f', 'g', $node->attr('uID'));
        }
        return $this->matchProviderId;
    }
}