<?php

namespace App\Processor\Provider\Opta\Xml;

use AsResultados\OAMBundle\Api\Internal\Results\Category\CategoryInterface;
use AsResultados\OAMBundle\Api\Internal\Results\RequestsResourceItem;
use AsResultados\OAMBundle\Api\Internal\Results\TeamCompetitionSeason\Request as TeamCompetitionSeasonRequest;
use AsResultados\OAMBundle\Api\Internal\Provider\Mapping\MappingInterface;
use AsResultados\OAMBundle\Api\Internal\Results\Competition\Register as CompetitionRegister;
use AsResultados\OAMBundle\Api\Internal\Results\CompetitionSeason\Register as CompetitionSeasonRegister;
use AsResultados\OAMBundle\Api\Internal\Results\TeamCompetitionSeason\Register as TeamCompetitionSeasonRegister;
use AsResultados\OAMBundle\Api\Internal\Results\Team\Register as TeamRegister;
use AsResultados\OAMBundle\Model\Results\Competition\Competition;
use AsResultados\OAMBundle\Model\Results\CompetitionSeason\CompetitionSeason;
use AsResultados\OAMBundle\Model\Collection\MappingCollection;
use AsResultados\OAMBundle\Model\Collection\Collection;
use AsResultados\OAMBundle\Model\Collection\ProviderIdsCollection;
use AsResultados\OAMBundle\Model\Results\CompetitionSeason\Embed\Honours;
use AsResultados\OAMBundle\Model\Results\TeamCompetitionSeason\Embed\Name;
use AsResultados\OAMBundle\Model\Results\TeamCompetitionSeason\TeamCompetitionSeason;
use AsResultados\OAMBundle\Exception\MappingException;
use AsResultados\OAMBundle\Exception\EmptyItemException;
use App\Processor\Provider\Traits\TeamTrait;
use Exception;
use Symfony\Component\DomCrawler\Crawler;

class Trophies extends AbstractProcessor
{
    use TeamTrait;

    /**
     * @var string
     */
    protected $teamProviderId;

    protected function setProcessorVariablesFromMapping(): void
    {
        $mappingCollection = MappingCollection::getInstance();
        $this->setTeam(
            $mappingCollection->get(MappingInterface::ENTITY_TEAM, self::PROVIDER, $this->getTeamIdFromXml()),
            $this->getClient()
        );
        $this->setMappingsOwn($this->getMappingsOwn());
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function run(): bool
    {
        //Add non existent competitions
        $data = $this->getCompetitionsFromXml();
        if (count($data->getAllUnRegistered()) > 0) {
            try {
                $register = CompetitionRegister::getInstance($this->getClient());
                $register->postWithMapping(
                    $data->getAllUnRegistered(), $data->getUnRegisteredIds(), MappingInterface::ENTITY_COMPETITION, self::PROVIDER
                );
            } catch (EmptyItemException $e) {
            } catch (Exception $e) {
                $this->getLogger()->error('Can not insert competitions: ' . $e->getMessage());
                throw $e;
            }
        }
        //Add non existent competitionsSeasons
        $data = $this->getCompetitionsSeasonsFromXml();
        if (count($data->getAllUnRegistered()) > 0) {
            try {
                $register = CompetitionSeasonRegister::getInstance($this->getClient());
                $register->postWithMapping(
                    $data->getAllUnRegistered(), $data->getUnRegisteredIds(), MappingInterface::ENTITY_COMPETITION_SEASON, self::PROVIDER
                );
            } catch (EmptyItemException $e) {
            } catch (Exception $e) {
                $this->getLogger()->error('Can not insert competitionsSeasons: ' . $e->getMessage());
                throw $e;
            }
        }
        //Add non existent teamsCompetitionSeason
        $data = $this->getTeamsCompetitionsSeasonsFromXml();
        $register = TeamCompetitionSeasonRegister::getInstance($this->getClient());
        if (count($data->getAllUnRegistered()) > 0) {
            try {
                $register->postWithMapping(
                    $data->getAllUnRegistered(), $data->getUnRegisteredIds(), MappingInterface::ENTITY_TEAM, self::PROVIDER
                );
            } catch (EmptyItemException $e) {
            } catch (Exception $e) {
                $this->getLogger()->error('Can not insert teamsCompetitionSeason: ' . $e->getMessage());
                throw $e;
            }
            try {
                //Update teamsCompetitionSeason inside team
                $team = $this->getTeam();
                $team->setCompetitionsSeasons($register->getLastInsertedIds());
                //Update team
                $register = TeamRegister::getInstance($this->getClient());
                $register->patch([$team]);
            } catch (EmptyItemException $e) {
            } catch (Exception $e) {
                $this->getLogger()->error('Can not update team to insert its competitionsSeasons: ' . $e->getMessage());
                throw $e;
            }
        }
        //Get honours after all structures insertions
        $honours = $this->getCompetitionsSeasonsHonoursFromXml();
        $competitionSeasonRegister = CompetitionSeasonRegister::getInstance($this->getClient());
        //Update competitions seasons
        try {
            $competitionSeasonRegister->patch($honours->getAllRegistered());
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->warning('Can not update competitions seasons honours: ' . $e->getMessage());
        }
        return true;
    }

    /**
     * @return ProviderIdsCollection[]
     * @throws Exception
     */
    protected function getMappingsFromProviderData(): array
    {
        $mappings = array();
        $mappings[] = $this->getMappingCompetitionsFromXml();
        $mappings[] = $this->getMappingCompetitionsSeasonsFromXml();
        $mappings[] = $this->getMappingTeamsFromXml();
        return $mappings;
    }

    /**
     * @throws Exception
     */
    protected function getMappingsOwn(): array
    {
        $requests = array();
        $mappingCollection = MappingCollection::getInstance();
        $request = new TeamCompetitionSeasonRequest();
        $requests[] = new RequestsResourceItem(
            $request->getByTeams(
                $mappingCollection->getMultiple(MappingInterface::ENTITY_TEAM, self::PROVIDER)
            ),
            $request->getResource()
        );
        return $requests;
    }

    /**
     * @return Collection
     * @throws Exception
     */
    protected function getCompetitionsFromXml(): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $result = new Collection(Competition::class);
        $this->getCrawledXmlDocument()
            ->filterXPath('trophies/competition[trophy/@winnerContestantOpId="' . substr($this->getTeamIdFromXml(), 1) . '"]')
            ->each(function (Crawler $node) use (&$result, $mappingCollection) {
                try {
                    $id = $node->attr('opId');
                    if (empty($id)) {
                        $this->getLogger()->warning('Skipping competition: ' . $node->attr('name') . ' - Do not have id');
                        return;
                    }
                    $item = $this->createCompetitionFromXmlNode($node);
                    if ($mappingCollection->exists($mappingCollection::ENTITY_COMPETITION, self::PROVIDER, $id)) {
                        $item->setId($mappingCollection->get($mappingCollection::ENTITY_COMPETITION, self::PROVIDER, $id));
                        $result->addRegistered($item);
                    } else {
                        $result->addUnRegistered($item, $id);
                    }
                } catch (Exception $e) {
                    //Notify but keep going
                    $this->getLogger()->warning('Skipping item competition: ' . $e->getMessage());
                }
            });
        return $result;
    }

    /**
     * @param Crawler $node
     * @return Competition
     * @throws Exception
     */
    protected function createCompetitionFromXmlNode(Crawler $node): Competition
    {
        $competition = new Competition();
        $competition->setName($node->attr('name'));
        $team = $this->getCrawledXmlDocument()->filterXPath('trophies/contestant');
        //We assume all the trophies are official
        $competition->setFriendly(false);
        $competition->setTypeById($this->getCompetitionType($team->attr('type')));
        $competition->setGender($this->getGender($team->attr('teamType')));
        return $competition;
    }

    /**
     * @return Collection
     * @throws Exception
     */
    protected function getCompetitionsSeasonsFromXml(): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $result = new Collection(CompetitionSeason::class);
        $this->getCrawledXmlDocument()
            ->filterXPath('trophies/competition/trophy[@winnerContestantOpId="' . substr($this->getTeamIdFromXml(), 1) . '"]')
            ->each(function (Crawler $node) use (&$result, $mappingCollection) {
                try {
                    $competitionId = $node->parents()->attr('opId');
                    if (empty($competitionId)) {
                        throw new EmptyItemException('competition id', 'Competition node with name: ' . $node->parents()->attr('name'));
                    }
                    $item = $this->createCompetitionSeasonFromXmlNode($node);
                    $id = $this->generateProviderCompetitionSeasonId($competitionId, $item->getSeason());
                    if (!$mappingCollection->exists($mappingCollection::ENTITY_COMPETITION_SEASON, self::PROVIDER, $id)) {
                        $result->addUnRegistered($item, $id);
                    }
                } catch (Exception $e) {
                    //Notify but keep going
                    $this->getLogger()->warning('Skipping item competition season: ' . $e->getMessage());
                }
            });
        return $result;
    }

    /**
     * @param Crawler $node
     * @return CompetitionSeason
     * @throws EmptyItemException
     * @throws MappingException
     */
    protected function createCompetitionSeasonFromXmlNode(Crawler $node): CompetitionSeason
    {
        $mappingCollection = MappingCollection::getInstance();
        $competitionSeason = new CompetitionSeason();
        $competitionId = $node->parents()->attr('opId');
        if (empty($competitionId)) {
            throw new EmptyItemException('competition id', 'Competition node with name: ' . $node->parents()->attr('name'));
        }
        $competitionSeason->setCompetitionById($mappingCollection->get($mappingCollection::ENTITY_COMPETITION, self::PROVIDER, $competitionId));
        $seasonId = $node->attr('tournamentCalendarName');
        //Season id can be written in two ways:
        //1) a year (2015)
        //2) a mix of years (2015/2016)
        $seasonId = preg_match('/(\d)+/', $seasonId, $seasonIdYears);
        //Error or lack of matching
        if ($seasonId !== 1) {
            throw new EmptyItemException('season id', 'Trophy node with name: ' . $node->attr('tournamentCalendarName'));
        }
        $competitionSeason->setSeason($seasonIdYears[0]);
        $competitionSeason->setName($node->parents()->attr('name') . ' ' . $node->attr('tournamentCalendarName'));
        return $competitionSeason;
    }

    /**
     * @return Collection
     * @throws Exception
     */
    protected function getTeamsCompetitionsSeasonsFromXml(): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $result = new Collection(TeamCompetitionSeason::class);
        $this->getCrawledXmlDocument()
            ->filterXPath('trophies/competition/trophy[@winnerContestantOpId="' . substr($this->getTeamIdFromXml(), 1) . '"]')
            ->each(function (Crawler $node) use (&$result, $mappingCollection) {
                try {
                    //We only need teamsCompetitionSeason which are not in the system
                    $item = $this->createTeamCompetitionSeasonFromXmlNode($node);
                    if (!$mappingCollection->existsOwn(TeamCompetitionSeason::class, $item->getUniqueIdByRelations())) {
                        $result->addUnRegistered($item, $item->getUniqueIdByRelations());
                    }
                } catch (MappingException | EmptyItemException $e) {
                    $this->getLogger()->warning('Skipping teamCompetitionSeason: ' . $e->getMessage());
                }
            });
        return $result;
    }

    /**
     * @param Crawler $node
     * @return TeamCompetitionSeason
     * @throws EmptyItemException
     * @throws MappingException
     * @throws Exception
     */
    protected function createTeamCompetitionSeasonFromXmlNode(Crawler $node): TeamCompetitionSeason
    {
        $mappingCollection = MappingCollection::getInstance();
        $teamCompetitionSeason = new TeamCompetitionSeason();
        $competitionId = $node->parents()->attr('opId');
        if (empty($competitionId)) {
            throw new EmptyItemException('competition id', 'Competition node with name: ' . $node->parents()->attr('name'));
        }
        $seasonId = $node->attr('tournamentCalendarName');
        //Season id can be written in two ways:
        //1) a year (2015)
        //2) a mix of years (2015/2016)
        $seasonId = preg_match('/(\d)+/', $seasonId, $seasonIdYears);
        //Error or lack of matching
        if ($seasonId !== 1) {
            throw new EmptyItemException('season id', 'Trophy node with name: ' . $node->attr('tournamentCalendarName'));
        }
        $teamCompetitionSeason->setCompetitionSeasonById(
            $mappingCollection->get(
                MappingInterface::ENTITY_COMPETITION_SEASON,
                self::PROVIDER,
                $this->generateProviderCompetitionSeasonId($competitionId, $seasonIdYears[0])
            )
        );
        $teamCompetitionSeason->setTeamById($this->getTeam()->getId());
        $name = new Name();
        $name->setKnown($node->attr('winnerContestantName'));
        $teamCompetitionSeason->setName($name);
        return $teamCompetitionSeason;
    }

    /**
     * @return Collection
     * @throws Exception
     */
    protected function getCompetitionsSeasonsHonoursFromXml(): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $result = new Collection(CompetitionSeason::class);
        $this->getCrawledXmlDocument()
            ->filterXPath('trophies/competition/trophy[@winnerContestantOpId="' . substr($this->getTeamIdFromXml(), 1) . '"]')
            ->each(function (Crawler $node) use (&$result, $mappingCollection) {
                try {
                    $competitionId = $node->parents()->attr('opId');
                    if (empty($competitionId)) {
                        throw new EmptyItemException('competition id', 'Competition node with name: ' . $node->parents()->attr('name'));
                    }
                    $seasonId = $node->attr('tournamentCalendarName');
                    //Season id can be written in two ways:
                    //1) a year (2015)
                    //2) a mix of years (2015/2016)
                    $seasonId = preg_match('/(\d)+/', $seasonId, $seasonIdYears);
                    //Error or lack of matching
                    if ($seasonId !== 1) {
                        throw new EmptyItemException('season id', 'Trophy node with name: ' . $node->attr('tournamentCalendarName'));
                    }
                    $seasonId = $seasonIdYears[0];
                    $id = $this->generateProviderCompetitionSeasonId($competitionId, $seasonId);
                    //We only need existent ones because we are only going to update (we have previously inserted the new ones)
                    if ($mappingCollection->exists($mappingCollection::ENTITY_COMPETITION_SEASON, self::PROVIDER, $id)) {
                        $item = $this->createCompetitionSeasonHonoursFromXmlNode($node);
                        $item->setId($mappingCollection->get($mappingCollection::ENTITY_COMPETITION_SEASON, self::PROVIDER, $id));
                        $result->addRegistered($item);
                    }
                } catch (Exception $e) {
                    $this->getLogger()->warning('Skipping item competition season honours: ' . $e->getMessage());
                }
            });
        return $result;
    }

    /**
     * @param Crawler $node
     * @return CompetitionSeason
     */
    protected function createCompetitionSeasonHonoursFromXmlNode(Crawler $node): CompetitionSeason
    {
        $competitionSeason = new CompetitionSeason();
        $honours = new Honours();
        $honours->setFirst($this->getTeam()->getId());
        $competitionSeason->setHonours($honours);
        return $competitionSeason;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingCompetitionsFromXml(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_COMPETITION, self::PROVIDER);
        $this->getCrawledXmlDocument()
            ->filterXPath('trophies/competition')
            ->each(function (Crawler $node) use ($collection) {
                $id = $node->attr('opId');
                //No id, skip it
                if (empty($id)) {
                    return;
                }
                $collection->addId($id);
            });
        return $collection;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingCompetitionsSeasonsFromXml(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_COMPETITION_SEASON, self::PROVIDER);
        $this->getCrawledXmlDocument()
            ->filterXPath('trophies/competition/trophy')
            ->each(function (Crawler $node) use ($collection) {
                $competitionId = $node->parents()->attr('opId');
                //No id, skip it
                if (empty($competitionId)) {
                    return;
                }
                $seasonId = $node->attr('tournamentCalendarName');
                //Season id can be written in two ways:
                //1) a year (2015)
                //2) a mix of years (2015/2016)
                $seasonId = preg_match('/(\d)+/', $seasonId, $seasonIdYears);
                //Error or lack of matching
                if ($seasonId !== 1) {
                    return;
                }
                $collection->addId($this->generateProviderCompetitionSeasonId($competitionId, $seasonIdYears[0]));
            });
        return $collection;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingTeamsFromXml(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_TEAM, self::PROVIDER);
        $collection->addId($this->getTeamIdFromXml());
        return $collection;
    }

    /**
     * @return string
     */
    protected function getTeamIdFromXml(): string
    {
        if (!isset($this->teamProviderId)) {
            $node = $this->getCrawledXmlDocument()->filterXPath('trophies/contestant')->first();
            $this->teamProviderId = 't' . $node->attr('opId');
        }
        return $this->teamProviderId;
    }

    /**
     * @param string $type
     * @return string
     * @throws Exception
     */
    private final function getCompetitionType(string $type): string
    {
        switch (strtolower($type)) {
            case 'club':
                return CategoryInterface::COMPETITION_TYPE_CLUB;
            case 'national':
                return CategoryInterface::COMPETITION_TYPE_NATIONAL_TEAM;
            default:
                throw new Exception('Competition type not recognized');
        }
    }

    /**
     * @param string $type
     * @return string
     */
    private final function getGender(string $type): string
    {
        switch (strtolower($type)) {
            case 'women':
                return 'FEMALE';
            case 'men':
            default:
                return 'MALE';
        }
    }
}