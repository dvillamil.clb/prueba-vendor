<?php

namespace App\Processor\Provider\Opta\Xml\MultipleCompetitionSeasons;

use App\Processor\Provider\Opta\Xml\F30 as F30Master;
use AsResultados\OAMBundle\Client\ClientManager;
use AsResultados\OAMBundle\Log\Logger;
use Exception;
use Gaufrette\Filesystem;

class F30 extends F30Master
{
    public function __construct(Filesystem $fileSystem, ClientManager $client, Logger $logger, string $filePath)
    {
        parent::__construct($fileSystem, $client, $logger, $filePath);
        throw new Exception('F30 is not allowed for MultipleCompetitionSeasons');
    }
}