<?php

namespace App\Processor\Provider\Opta\Xml\MultipleCompetitionSeasons;

use App\Processor\Provider\Opta\Xml\F40 as F40Master;
use AsResultados\OAMBundle\Api\Internal\Provider\Mapping\MappingInterface;
use AsResultados\OAMBundle\Exception\MissingItemException;
use AsResultados\OAMBundle\Model\Collection\ProviderIdsCollection;
use AsResultados\OAMBundle\Model\Collection\MappingCollection;
use Exception;
use Symfony\Component\DomCrawler\Crawler;

class F40 extends F40Master
{
    /**
     * @inheritDoc
     */
    protected function setProcessorVariablesFromMapping(): void
    {
        $mappingCollection = MappingCollection::getInstance();
        $this->setCompetitionSeasonId($mappingCollection->get(
            MappingInterface::ENTITY_COMPETITION_SEASON, self::PROVIDER, $this->getCompetitionSeasonFromXml()
        ));
        $this->setCompetitionSeasonsByStageIds(
            $mappingCollection->getMultiple(MappingInterface::ENTITY_STAGE, self::PROVIDER),
            $this->getClient()
        );
        $this->setMappingsOwn($this->getMappingsOwn());
    }

    /**
     * @inheritDoc
     */
    protected function getMappingsFromProviderData(): array
    {
        $mappings = parent::getMappingsFromProviderData();
        $mappings[] = $this->getMappingStagesFromXml();
        return $mappings;
    }

    /**
     * @return ProviderIdsCollection
     * @throws Exception
     */
    protected function getMappingStagesFromXml(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_STAGE, self::PROVIDER);
        $this->getCrawledXmlDocument($this->getFileSystemCalendarPath($this->getCompetitionFromXml(), $this->getSeasonFromXml()))
            ->filterXPath('SoccerFeed/SoccerDocument/MatchData/MatchInfo')
            ->each(function (Crawler $node) use ($collection) {
                $stageId = $this->generateProviderStageId(
                    $this->getCompetitionSeasonFromXml(),
                    $node->attr('RoundType'),
                    $node->attr('RoundNumber'));
                $collection->addId($stageId);
            });
        return $collection;
    }

    /**
     * @inheritDoc
     */
    protected function getTeamCompetitionSeasons(string $id): array
    {
        if (!isset($this->teamsInCompetitionSeasons)) {
            $mappingCollection = MappingCollection::getInstance();
            $result = array();
            $this->getCrawledXmlDocument($this->getFileSystemCalendarPath($this->getCompetitionFromXml(), $this->getSeasonFromXml()))
                ->filterXPath('SoccerFeed/SoccerDocument/MatchData/MatchInfo')
                ->each(function (Crawler $node) use (&$result, $mappingCollection) {
                    $stageId = $this->generateProviderStageId(
                        $this->getCompetitionSeasonFromXml(),
                        $node->attr('RoundType'),
                        $node->attr('RoundNumber'));
                    if (!$mappingCollection->exists(
                        MappingInterface::ENTITY_STAGE,
                        self::PROVIDER,
                        $stageId)) {
                        //Do not have mapping for the stage, very strange at this point, but keep going (F1 will log this)
                        return;
                    }
                    $competitionSeasonId = $this->getCompetitionSeasonIdFromStageId(
                        $mappingCollection->get(
                            MappingInterface::ENTITY_STAGE,
                            self::PROVIDER,
                            $stageId)
                    );
                    $node->parents()->filterXPath('MatchData/TeamData[@TeamRef]')->each(function (Crawler $nodeTeam) use (&$result, $mappingCollection, $competitionSeasonId) {
                        if (!$mappingCollection->exists($mappingCollection::ENTITY_TEAM, self::PROVIDER, $nodeTeam->attr('TeamRef'))) {
                            //We do not have this team mapping, very strange but keep going
                            return;
                        }
                        $id = $mappingCollection->get($mappingCollection::ENTITY_TEAM, self::PROVIDER, $nodeTeam->attr('TeamRef'));
                        if (!isset($result[$id][$competitionSeasonId])) {
                            $result[$id][$competitionSeasonId] = $competitionSeasonId;
                        }
                    });
                });
            if (empty($result)) {
                throw new Exception('There are not teams in calendar provider file');
            }
            $this->teamsInCompetitionSeasons = $result;
        }
        if (!isset($this->teamsInCompetitionSeasons[$id])) {
            throw new MissingItemException($id, 'teamsInCompetitionSeason', 'can not associate team id to any competitionSeason');
        }
        return $this->teamsInCompetitionSeasons[$id];
    }
}