<?php

namespace App\Processor\Provider\Opta\Xml;

use App\Event\ResourceProcessed\Ranking\RankingPersonEvent;
use App\Event\ResourceProcessed\Ranking\RankingTeamEvent;
use App\Event\ResourceProcessed\Team\TeamStatsEvent;
use App\Processor\Provider\Traits\CompetitionSeasonsTrait;
use AsResultados\OAMBundle\Api\Internal\Provider\Mapping\MappingInterface;
use AsResultados\OAMBundle\Api\Internal\Results\Category\CategoryInterface;
use AsResultados\OAMBundle\Api\Internal\Results\TeamCompetitionSeason\Register as TeamRegister;
use AsResultados\OAMBundle\Api\Internal\Results\PersonCompetitionSeason\Register as PersonRegister;
use AsResultados\OAMBundle\Api\Internal\Results\RequestsResourceItem;
use AsResultados\OAMBundle\Exception\MissingItemException;
use AsResultados\OAMBundle\Model\Results\PersonCompetitionSeason\PersonCompetitionSeason;
use AsResultados\OAMBundle\Model\Results\TeamCompetitionSeason\Embed\Averages as TeamAverages;
use AsResultados\OAMBundle\Model\Results\TeamCompetitionSeason\Embed\Stat as TeamStat;
use AsResultados\OAMBundle\Model\Results\PersonCompetitionSeason\Embed\Averages as PersonAverages;
use AsResultados\OAMBundle\Model\Results\PersonCompetitionSeason\Embed\Stat as PersonStat;
use AsResultados\OAMBundle\Model\Results\TeamCompetitionSeason\TeamCompetitionSeason;
use AsResultados\OAMBundle\Model\Collection\MappingCollection;
use AsResultados\OAMBundle\Model\Collection\Collection;
use AsResultados\OAMBundle\Api\Internal\Results\TeamCompetitionSeason\Request as TeamCompetitionSeasonRequest;
use AsResultados\OAMBundle\Api\Internal\Results\PersonCompetitionSeason\Request as PersonCompetitionSeasonRequest;
use AsResultados\OAMBundle\Model\Collection\ProviderIdsCollection;
use AsResultados\OAMBundle\Exception\EmptyItemException;
use AsResultados\OAMBundle\Exception\MappingException;
use App\Processor\Provider\Traits\CompetitionSeasonIdTrait;
use Exception;
use Symfony\Component\DomCrawler\Crawler;

class F30 extends AbstractProcessor
{
    use CompetitionSeasonIdTrait;
    use CompetitionSeasonsTrait;

    /**
     * @inheritDoc
     */
    protected function setProcessorVariablesFromMapping(): void
    {
        $mappingCollection = MappingCollection::getInstance();
        $this->setCompetitionSeasonId($mappingCollection->get(
            MappingInterface::ENTITY_COMPETITION_SEASON, self::PROVIDER, $this->getCompetitionSeasonFromXml()
        ));
        $this->setCompetitionSeasons([$this->getCompetitionSeasonId()], $this->getClient());
        $this->setMappingsOwn($this->getMappingsOwn());
    }

    /**
     * @inheritDoc
     */
    public function run(): bool
    {
        //Teams stats
        $teams = $this->getTeamsStatsFromXml();
        $register = TeamRegister::getInstance($this->getClient());
        //Update teams stats (we only update because f40 is the one which creates the teamCompetitionSeason)
        try {
            //Patch instead of put because we do not have all the data
            $register->patch($teams->getAllRegistered());
            $opUpdateTeams = true;
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not update teams stats: ' . $e->getMessage());
        }
        //Player stats
        $players = $this->getPeopleStatsFromXml();
        $register = PersonRegister::getInstance($this->getClient());
        //Update players stats (we only update because f40 is the one which creates the personCompetitionSeason)
        try {
            //Patch instead of put because we do not have all the data
            $register->patch($players->getAllRegistered());
            $opUpdatePlayers = true;
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not update players stats: ' . $e->getMessage());
        }
        //Dispatch events
        if (isset($opUpdateTeams)) {
            $this->dispatchEventsTeam();
        }
        if (isset($opUpdatePlayers)) {
            $this->dispatchEventsPlayer();
        }
        return true;
    }

    private function dispatchEventsTeam(): void
    {
        $event = new RankingTeamEvent();
        $event->setCompetitionSeason($this->getCompetitionSeasonId());
        $this->dispatcher->dispatch($event, $event->getEventName());
        $mappingCollection = MappingCollection::getInstance();
        foreach ($this->getCompetitionSeasons() as $competitionSeason) {
            foreach ($mappingCollection->getMultiple($mappingCollection::ENTITY_TEAM, self::PROVIDER) as $item) {
                //Events team calendar
                $event = new TeamStatsEvent();
                $event->setTeam($item);
                $event->setSeason($competitionSeason->getSeason()->getId());
                $this->dispatcher->dispatch($event, $event->getEventName());
            }
            //Only one competitionSeason
            break;
        }
    }

    private function dispatchEventsPlayer(): void
    {
        $event = new RankingPersonEvent();
        $event->setCompetitionSeason($this->getCompetitionSeasonId());
        $this->dispatcher->dispatch($event, $event->getEventName());
    }

    /**
     * @inheritDoc
     */
    protected function getMappingsFromProviderData(): array
    {
        $mappings = array();
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_COMPETITION_SEASON, self::PROVIDER);
        $collection->addId($this->getCompetitionSeasonFromXml());
        $mappings[] = $collection;
        $mappings[] = $this->getMappingTeamsFromXml();
        $mappings[] = $this->getMappingPeopleFromXml();
        $mappings[] = new ProviderIdsCollection(MappingInterface::ENTITY_STAT_TEAM, self::PROVIDER, true);
        $mappings[] = new ProviderIdsCollection(MappingInterface::ENTITY_STAT_PLAYER, self::PROVIDER, true);
        return $mappings;
    }

    /**
     * @throws Exception
     */
    protected function getMappingsOwn(): array
    {
        $requests = array();
        $mappingCollection = MappingCollection::getInstance();
        $request = new TeamCompetitionSeasonRequest();
        $requests[] = new RequestsResourceItem(
            $request->getByTeamsAndCompetitionSeason(
                $mappingCollection->getMultiple(MappingInterface::ENTITY_TEAM, self::PROVIDER),
                $this->getCompetitionSeasonId()),
            $request->getResource()
        );
        $request = new PersonCompetitionSeasonRequest();
        $requests[] = new RequestsResourceItem(
            $request->getByPeopleAndCompetitionSeason(
                $mappingCollection->getMultiple(MappingInterface::ENTITY_PERSON, self::PROVIDER),
                $this->getCompetitionSeasonId()),
            $request->getResource()
        );
        return $requests;
    }

    /**
     * @return Collection
     * @throws Exception
     */
    protected function getTeamsStatsFromXml(): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $result = new Collection(TeamCompetitionSeason::class);
        $this->getCrawledXmlDocument()
            ->filterXPath('SeasonStatistics/Team')
            ->each(function (Crawler $node) use (&$result, $mappingCollection) {
                $id = $node->attr('id');
                //For unknown reason opta changes the id in the stats and remove the first t they use in f40
                $id = 't' . $id;
                try {
                    $team = new TeamCompetitionSeason();
                    $team->setCompetitionSeasonById($this->getCompetitionSeasonId());
                    $team->setTeamById($mappingCollection->get(
                        $mappingCollection::ENTITY_TEAM,
                        self::PROVIDER,
                        $id
                    ));
                    //First we retrieve games played to do averages
                    $gamesPlayed = $node->filterXPath('Team/Stat[@name="Games Played"]');
                    if ($gamesPlayed->count() > 0) {
                        $gamesPlayed = (int)$gamesPlayed->text();
                    } else {
                        $gamesPlayed = null;
                    }
                    $stats = array();
                    $node->filterXPath('Team/Stat')
                        ->each(function (Crawler $node) use (&$stats, $mappingCollection, $gamesPlayed) {
                            if (!$mappingCollection->exists(
                                $mappingCollection::ENTITY_STAT_TEAM,
                                self::PROVIDER,
                                $node->attr('name')
                            )) {
                                //We do not have mapping for that stat, keep going
                                return;
                            }
                            $stat = new TeamStat();
                            $stat->setStatById($mappingCollection->get(
                                $mappingCollection::ENTITY_STAT_TEAM,
                                self::PROVIDER,
                                $node->attr('name')
                            ));
                            $stat->setValue($node->text());
                            //Averages
                            if (!empty($gamesPlayed)) {
                                $averages = new TeamAverages();
                                $averages->setPerGame(round($node->text() / $gamesPlayed, self::STATS_FLOAT_PRECISION));
                                $stat->setAverages($averages);
                            }
                            //Add stat
                            $stats[] = $stat;
                        });
                    $team->setStats($stats);
                    if ($mappingCollection->existsOwn(TeamCompetitionSeason::class, $team->getUniqueIdByRelations())) {
                        $team->setId($mappingCollection->getOwn(TeamCompetitionSeason::class, $team->getUniqueIdByRelations()));
                        $result->addRegistered($team);
                    } else {
                        $result->addUnRegistered($team, $team->getUniqueIdByRelations());
                    }
                } catch (MappingException $e) {
                    //Mapping error, skip item and keep going
                }
            });
        return $result;
    }

    /**
     * @return Collection
     * @throws Exception
     */
    protected function getPeopleStatsFromXml(): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $result = new Collection(PersonCompetitionSeason::class);
        $this->getCrawledXmlDocument()
            ->filterXPath('SeasonStatistics/Team/Player')
            ->each(function (Crawler $node) use (&$result, $mappingCollection) {
                $id = $node->attr('player_id');
                //For unknown reason opta changes the id in the stats and remove the first p they use in f40
                $id = 'p' . $id;
                try {
                    $person = new PersonCompetitionSeason();
                    $person->setCompetitionSeasonById($this->getCompetitionSeasonId());
                    $person->setTypeById(CategoryInterface::PERSON_TYPE_PLAYER);
                    $person->setPersonById($mappingCollection->get(
                        $mappingCollection::ENTITY_PERSON,
                        self::PROVIDER,
                        $id
                    ));
                    //For unknown reason opta changes the id in the stats and remove the first t they use in f40 in the teams
                    $teamId = 't' . $node->parents()->attr('id');
                    $person->setTeamById($mappingCollection->get(
                        $mappingCollection::ENTITY_TEAM,
                        self::PROVIDER,
                        $teamId
                    ));
                    $statsData = $node->filterXPath('Player/CurrentTeamOnly/Stat');
                    if ($statsData->count() <= 0) {
                        $statsData = $node->filterXPath('Player/Stat');
                    }
                    $stats = $this->getPersonStatsFromXml($statsData);
                    if (empty($stats)) {
                        throw new EmptyItemException('STATS', 'Player id: ' . $id);
                    }
                    $person->setStats($stats);
                    if ($mappingCollection->existsOwn(PersonCompetitionSeason::class, $person->getUniqueIdByRelations())) {
                        $person->setId($mappingCollection->getOwn(PersonCompetitionSeason::class, $person->getUniqueIdByRelations()));
                        $result->addRegistered($person);
                    } else {
                        $result->addUnRegistered($person, $person->getUniqueIdByRelations());
                    }
                } catch (MappingException $e) {
                    //Mapping error, skip item and keep going
                } catch (EmptyItemException $e) {
                    //No stats in a player, keep going
                }
            });
        return $result;
    }

    /**
     * @param Crawler $node
     * @return array
     */
    protected function getPersonStatsFromXml(Crawler $node): array
    {
        $mappingCollection = MappingCollection::getInstance();
        $stats = array();
        //First we retrieve games played and minutes to do averages
        $gamesPlayed = $node->filterXPath('Stat[@name="Games Played"]');
        if ($gamesPlayed->count() > 0) {
            $gamesPlayed = (int)$gamesPlayed->text();
        } else {
            $gamesPlayed = null;
        }
        $minutesPlayed = $node->filterXPath('Stat[@name="Time Played"]');
        if ($minutesPlayed->count() > 0) {
            $minutesPlayed = (int)$minutesPlayed->text();
        } else {
            $minutesPlayed = null;
        }
        $node->each(function (Crawler $node) use (&$stats, $mappingCollection, $gamesPlayed, $minutesPlayed) {
            if (!$mappingCollection->exists(
                $mappingCollection::ENTITY_STAT_PLAYER,
                self::PROVIDER,
                $node->attr('name')
            )) {
                //We do not have mapping for that stat, keep going
                return;
            }
            $stat = new PersonStat();
            $stat->setStatById($mappingCollection->get(
                $mappingCollection::ENTITY_STAT_PLAYER,
                self::PROVIDER,
                $node->attr('name')
            ));
            $stat->setValue($node->text());
            //Averages
            $setAverages = false;
            $averages = new PersonAverages();
            if (!empty($gamesPlayed)) {
                $averages->setPerGame(round($node->text() / $gamesPlayed, self::STATS_FLOAT_PRECISION));
                $setAverages = true;
            }
            if (!empty($minutesPlayed)) {
                if (!empty($node->text())) {
                    $averages->setMinutesToHappen(round($minutesPlayed / $node->text(), self::STATS_FLOAT_PRECISION));
                    $setAverages = true;
                }
            }
            if ($setAverages === true) {
                $stat->setAverages($averages);
            }
            //Add stat
            $stats[] = $stat;
        });
        return $stats;
    }


    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingTeamsFromXml(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_TEAM, self::PROVIDER);
        $this->getCrawledXmlDocument()
            ->filterXPath('SeasonStatistics/Team[@id]')
            ->each(function (Crawler $node) use ($collection) {
                //For unknown reason opta do not include the letter t at the beginning of the id, we add it manually
                $id = 't' . $node->attr('id');
                $collection->addId($id);
            });
        return $collection;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingPeopleFromXml(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_PERSON, self::PROVIDER);
        $this->getCrawledXmlDocument()
            ->filterXPath('SeasonStatistics/Team/Player[@player_id]')
            ->each(function (Crawler $node) use ($collection) {
                //For unknown reason opta do not include the letter p at the beginning of the id, we add it manually
                $id = 'p' . $node->attr('player_id');
                $collection->addId($id);
            });
        return $collection;
    }

    /**
     * @return string
     * @throws MissingItemException
     */
    protected function getCompetitionFromXml(): string
    {
        if (!isset($this->competitionProviderId)) {
            $node = $this->getCrawledXmlDocument()->filterXPath('SeasonStatistics');
            if (is_null($node->attr('competition_id'))) {
                throw new MissingItemException('competition_id', 'xml node', 'provider xml file');
            }
            $this->competitionProviderId = (string)$node->attr('competition_id');
        }
        return $this->competitionProviderId;
    }

    /**
     * @return string
     * @throws MissingItemException
     */
    protected function getSeasonFromXml(): string
    {
        if (!isset($this->seasonProviderId)) {
            $node = $this->getCrawledXmlDocument()->filterXPath('SeasonStatistics');
            if (is_null($node->attr('season_id'))) {
                throw new MissingItemException('season_id', 'xml node', 'provider xml file');
            }
            $this->seasonProviderId = (string)$node->attr('season_id');
        }
        return $this->seasonProviderId;
    }
}