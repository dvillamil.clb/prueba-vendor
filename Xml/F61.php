<?php

namespace App\Processor\Provider\Opta\Xml;

use App\Utils\StringTrait;
use AsResultados\OAMBundle\Api\Internal\Provider\Mapping\MappingInterface;
use AsResultados\OAMBundle\Api\Internal\Results\Tv\Register as TvRegister;
use AsResultados\OAMBundle\Exception\DatabaseInconsistencyException;
use AsResultados\OAMBundle\Exception\EmptyItemException;
use AsResultados\OAMBundle\Exception\MappingException;
use AsResultados\OAMBundle\Exception\MissingItemException;
use AsResultados\OAMBundle\Model\Collection\Collection;
use AsResultados\OAMBundle\Model\Collection\MappingCollection;
use AsResultados\OAMBundle\Model\Collection\ProviderIdsCollection;
use AsResultados\OAMBundle\Model\Results\Match\Match;
use AsResultados\OAMBundle\Model\Results\Tv\Tv;
use Exception;
use Symfony\Component\DomCrawler\Crawler;

class F61 extends F1
{
    use StringTrait;

    /**
     * @inheritDoc
     */
    public function run(): bool
    {
        $tvs = $this->getTvsFromXml();
        //Insert tvs
        $tvRegister = TvRegister::getInstance($this->getClient());
        try {
            $tvRegister->postWithMapping(
                $tvs->getAllUnRegistered(), $tvs->getUnRegisteredIds(), MappingInterface::ENTITY_TV, self::PROVIDER
            );
        } catch (DatabaseInconsistencyException $e) {
            $this->getLogger()->emergency('Can not insert tvs: ' . $e->getMessage());
            throw $e;
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not insert tvs: ' . $e->getMessage());
        }
        //Update tvs
        try {
            $tvRegister->patch($tvs->getAllRegistered());
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not update tvs: ' . $e->getMessage());
        }
        return parent::run();
    }

    /**
     * @inheritDoc
     */
    protected function getMappingsFromProviderData(): array
    {
        $mappings = parent::getMappingsFromProviderData();
        $mappings[] = $this->getMappingTvsFromXml();
        return $mappings;
    }

    /**
     * @param Crawler $node
     * @return Match
     * @throws MappingException
     * @throws MissingItemException
     * @throws Exception
     */
    protected function createMatchFromXmlNode(Crawler $node): Match
    {
        $mappingCollection = MappingCollection::getInstance();
        $match = parent::createMatchFromXmlNode($node);
        $tvsXml = $node->attr('tv_id');
        $tvsXml = explode(',', $tvsXml);
        $tvs = [];
        foreach ($tvsXml as $tvXml) {
            //Opta delivers 1 when they do not know yet the tv, so we skip it
            if ((int)$tvXml === 1) {
                continue;
            }
            if ($mappingCollection->exists($mappingCollection::ENTITY_TV, self::PROVIDER, $tvXml)) {
                $tvs[] = $mappingCollection->get($mappingCollection::ENTITY_TV, self::PROVIDER, $tvXml);
            }
        }
        //We add this always although it may be empty because maybe there has been an error and we have to eliminate
        $match->setTvs($tvs);
        return $match;
    }

    /**
     * @return Collection
     * @throws Exception
     */
    protected function getTvsFromXml(): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $result = new Collection(Tv::class);
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/TvTypes/TvType')
            ->each(function (Crawler $node) use (&$result, $mappingCollection) {
                $id = (int)$node->attr('tv_id');
                if (empty($id) || $id === 1) {
                    //No id, skip it. Or id=1 and opta delivers 1 when they do not know the tv yet so we have to skip it
                    return;
                }
                $item = $this->createTvFromXmlNode($node);
                if ($mappingCollection->exists($mappingCollection::ENTITY_TV, self::PROVIDER, $id)) {
                    $item->setId($mappingCollection->get($mappingCollection::ENTITY_TV, self::PROVIDER, $id));
                    //Set slug to null because it is already inserted
                    $item->setSlug(null);
                }
                if ($mappingCollection->exists($mappingCollection::ENTITY_TV, self::PROVIDER, $id)) {
                    $result->addRegistered($item);
                } else {
                    $result->addUnRegistered($item, $id);
                }
            });
        return $result;
    }

    /**
     * @param Crawler $node
     * @return Tv
     * @throws Exception
     */
    protected function createTvFromXmlNode(Crawler $node): Tv
    {
        $tv = new Tv();
        $tv->setName($node->attr('tv_name'));
        $tv->setAbbreviation($node->attr('tv_short_name'));
        $tv->setSlug($this->normalizeString($tv->getName()));
        return $tv;
    }


    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingTvsFromXml(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_TV, self::PROVIDER);
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/TvTypes/TvType[@tv_id]')
            ->each(function (Crawler $node) use ($collection) {
                //Opta delivers 1 when they do not know yet the tv, so we skip it
                if ((int)$node->attr('tv_id') === 1) {
                    return;
                }
                $collection->addId($node->attr('tv_id'));
            });
        return $collection;
    }
}