<?php

namespace App\Processor\Provider\Opta\Xml;

use App\Event\ResourceProcessed\Team\TeamCalendarEvent;
use AsResultados\OAMBundle\Api\Internal\Provider\Mapping\MappingInterface;
use AsResultados\OAMBundle\Api\Internal\Provider\Mapping\Resource as MappingResource;
use AsResultados\OAMBundle\Api\Internal\Results\Category\CategoryInterface;
use AsResultados\OAMBundle\Api\Internal\Results\Match\Resource as MatchResource;
use AsResultados\OAMBundle\Api\Internal\Results\Match\Register as MatchRegister;
use AsResultados\OAMBundle\Exception\DatabaseInconsistencyException;
use AsResultados\OAMBundle\Model\Collection\MappingCollection;
use AsResultados\OAMBundle\Model\Collection\Collection;
use AsResultados\OAMBundle\Model\Collection\PendingCollection;
use AsResultados\OAMBundle\Model\Results\Category\Category;
use AsResultados\OAMBundle\Model\Results\Match\Embed\Goal;
use AsResultados\OAMBundle\Model\Results\Match\Embed\Goals;
use AsResultados\OAMBundle\Model\Results\Match\Embed\PenaltiesShootOut;
use AsResultados\OAMBundle\Model\Results\Match\Embed\PenaltyShootOut;
use AsResultados\OAMBundle\Model\Results\Match\Embed\PersonRol;
use AsResultados\OAMBundle\Model\Results\Match\Embed\PreKnownTeams;
use AsResultados\OAMBundle\Model\Results\Match\Embed\Team;
use AsResultados\OAMBundle\Model\Results\Match\Match;
use AsResultados\OAMBundle\Model\Collection\ProviderIdsCollection;
use App\Event\ResourceProcessed\Calendar\CalendarEvent;
use App\Event\ResourceProcessed\Carousel\CarouselEvent;
use AsResultados\OAMBundle\Exception\EmptyItemException;
use AsResultados\OAMBundle\Exception\MappingException;
use AsResultados\OAMBundle\Exception\MissingItemException;
use App\Processor\Provider\Traits\CompetitionSeasonsTrait;
use App\Utils\Date;
use AsResultados\OAMBundle\Model\Results\Moment;
use DateTimeZone;
use Exception;
use Symfony\Component\DomCrawler\Crawler;

class F1 extends AbstractProcessor
{
    use CompetitionSeasonsTrait;

    private const DETAILS_ACCEPTED = array(
        CategoryInterface::MATCH_DETAIL_BASIC,
        CategoryInterface::MATCH_DETAIL_BASIC_GOALS
    );

    /**
     * @var array
     */
    private $matchesChartOrder = array();

    /**
     * @inheritDoc
     */
    protected function setProcessorVariablesFromMapping(): void
    {
        $mappingCollection = MappingCollection::getInstance();
        $stages = $mappingCollection->getMultiple(MappingInterface::ENTITY_STAGE, self::PROVIDER);
        //If we do not have stages mapping errors will be thrown to pending so we skip this
        if (!empty($stages)) {
            $this->setCompetitionSeasonsAndNumberOfMatchesInsertedAndDetailsByStageIds(
                $mappingCollection->getMultiple(MappingInterface::ENTITY_STAGE, self::PROVIDER),
                $this->getClient()
            );
        }
    }

    /**
     * @inheritDoc
     */
    public function run(): bool
    {
        //Chart order
        try {
            $this->matchesChartOrder = $this->getMatchesChartOrder();
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->warning('Can not calculate chart order: ' . $e->getMessage());
        }
        //Get matches
        $matches = $this->getMatchesFromXml();
        //Process each competitionSeason independently
        foreach ($matches as $competitionSeasonId => $collection) {
            try {
                $this->processMatchesFromCompetitionSeason($collection, $competitionSeasonId);
            } catch (DatabaseInconsistencyException $e) {
                throw $e;
            } catch (Exception $e) {
                //Try to process another competitionSeason
                continue;
            }
        }
        return true;
    }

    /**
     * @param Collection $matches
     * @param string $competitionSeasonId
     * @throws DatabaseInconsistencyException
     * @throws Exception
     */
    private function processMatchesFromCompetitionSeason(Collection $matches, string $competitionSeasonId): void
    {
        //Check structure
        try {
            $matchResource = new MatchResource();
            $matchResource->checkMatchesStructure(
                $matches->getAllUnRegistered(),
                $matches->getAllRegistered(),
                $this->getCompetitionSeason($competitionSeasonId),
                $this->getCompetitionSeasonMatchesInserted($competitionSeasonId)
            );
        } catch (Exception $e) {
            $this->getLogger()->critical(
                'Matches do not pass checking structure process for competitionSeason: ' . $competitionSeasonId . ' - ' . $e->getMessage(),
                [$this->getLogger()::CHANNEL_EDITORIAL]
            );
            throw $e;
        }
        //Insert matches
        $matchRegister = MatchRegister::getInstance($this->getClient());
        try {
            $matchRegister->postWithMapping(
                $matches->getAllUnRegistered(), $matches->getUnRegisteredIds(), MappingInterface::ENTITY_MATCH, self::PROVIDER
            );
            $opInsertMatches = true;
        } catch (DatabaseInconsistencyException $e) {
            $this->getLogger()->emergency('Can not insert matches for competitionSeason: ' . $competitionSeasonId . ' - ' . $e->getMessage());
            throw $e;
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not insert matches for competitionSeason: ' . $competitionSeasonId . ' - ' . $e->getMessage());
            throw $e;
        }
        //Update matches
        if (isset($opInsertMatches) && $this->matchesHaveRelatedMatchesIds()) {
            //Update mapping with last inserted items
            $mappingCollection = MappingCollection::getInstance();
            $mappingResource = new MappingResource();
            $mappings = $mappingResource->createMappingsItemFromModel(
                $matchRegister->getLastInsertedItems(),
                $matches->getUnRegisteredIds(),
                Match::class,
                self::PROVIDER
            );
            $mappingCollection->addMultiple($mappings);
            //We have to retrieve matches again because there is data which depends on matches being previously inserted
            $matchesUpdate = $this->getRegisteredMatchesFromCompetitionSeasonFromXml($competitionSeasonId);
            $matches->removeAllRegistered();
            $matches->addMultipleRegistered($matchesUpdate->getAllRegistered());
        }
        try {
            //Patch instead of put because we do not have all the data for the match in the f1
            $matchRegister->patch($matches->getAllRegistered());
            $opUpdateMatches = true;
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not update matches for competitionSeason: ' . $competitionSeasonId . ' - ' . $e->getMessage());
        }
        //Dispatch events
        if (isset($opInsertMatches) || isset($opUpdateMatches)) {
            $this->dispatchEvents($competitionSeasonId);
        }
    }

    /**
     * @param string $competitionSeasonId
     */
    private function dispatchEvents(string $competitionSeasonId): void
    {
        //Event calendar
        foreach ($this->getCompetitionSeasons() as $competitionSeason) {
            $event = new CalendarEvent();
            $event->setCompetitionSeason($competitionSeason->getId());
            $this->dispatcher->dispatch($event, $event->getEventName());
            //Events carousel
            $event = new CarouselEvent();
            $event->setCompetitionSeason($competitionSeasonId);
            $this->dispatcher->dispatch($event, $event->getEventName());
        }
        $mappingCollection = MappingCollection::getInstance();
        foreach ($this->getCompetitionSeasons() as $competitionSeason) {
            foreach ($mappingCollection->getMultiple($mappingCollection::ENTITY_TEAM, self::PROVIDER) as $item) {
                //Events team calendar
                $event = new TeamCalendarEvent();
                $event->setTeam($item);
                $event->setSeason($competitionSeason->getSeason()->getId());
                $this->dispatcher->dispatch($event, $event->getEventName());
            }
            //Only one competitionSeason
            break;
        }
    }

    /**
     * @inheritDoc
     */
    protected function getMappingsFromProviderData(): array
    {
        $mappings = array();
        $mappings[] = $this->getMappingStagesFromXml();
        $mappings[] = $this->getMappingMatchesFromXml();
        $mappings[] = $this->getMappingTeamsFromXml();
        $mappings[] = $this->getMappingPeopleFromXml();
        $mappings[] = $this->getMappingVenuesFromXml();
        $mappings[] = new ProviderIdsCollection(MappingInterface::ENTITY_MATCH_PERIOD, self::PROVIDER, true);
        $mappings[] = new ProviderIdsCollection(MappingInterface::ENTITY_MATCH_STATE, self::PROVIDER, true);
        $mappings[] = new ProviderIdsCollection(MappingInterface::ENTITY_PRE_KNOWN_TEAM, self::PROVIDER, true);
        $mappings[] = new ProviderIdsCollection(MappingInterface::ENTITY_ROL, self::PROVIDER, true);
        return $mappings;
    }

    /**
     * @return Collection[]
     * @throws Exception
     */
    protected function getMatchesFromXml(): array
    {
        $mappingCollection = MappingCollection::getInstance();
        $result = array();
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/MatchData')
            ->each(function (Crawler $node) use (&$result, $mappingCollection) {
                $id = $node->attr('uID');
                if (empty($id)) {
                    //No id, skip it
                    return;
                }
                try {
                    $item = $this->createMatchFromXmlNode($node);
                } catch (MappingException $e) {
                    $this->getLogger()->warning('Mapping error in match: ' . $id . ' - ' . $e->getMessage());
                    $pending = PendingCollection::getInstance();
                    $pending->addFromMapping($e, $this->getFilePath());
                    $this->getLogger()->warning(
                        'Mapping error added to pending - ' . $e->getMessage(),
                        [$this->getLogger()::CHANNEL_EDITORIAL]
                    );
                    return;
                } catch (MissingItemException $e) {
                    $this->getLogger()->warning('Missing item error in match: ' . $id . ' - ' . $e->getMessage());
                    return;
                }
                if (!isset($result[$item->getCompetitionSeason()->getId()])) {
                    $result[$item->getCompetitionSeason()->getId()] = new Collection(Match::class);
                }
                if ($mappingCollection->exists($mappingCollection::ENTITY_MATCH, self::PROVIDER, $id)) {
                    $item->setId($mappingCollection->get($mappingCollection::ENTITY_MATCH, self::PROVIDER, $id));
                    $result[$item->getCompetitionSeason()->getId()]->addRegistered($item);
                } else {
                    $result[$item->getCompetitionSeason()->getId()]->addUnRegistered($item, $id);
                }
            });
        return $result;
    }

    /**
     * @param string $competitionSeasonId
     * @return Collection
     */
    protected function getRegisteredMatchesFromCompetitionSeasonFromXml(string $competitionSeasonId): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $result = new Collection(Match::class);
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/MatchData')
            ->each(function (Crawler $node) use (&$result, $mappingCollection, $competitionSeasonId) {
                $id = $node->attr('uID');
                if (empty($id)) {
                    //No id, skip it
                    return;
                }
                try {
                    if (!$mappingCollection->exists($mappingCollection::ENTITY_MATCH, self::PROVIDER, $id)) {
                        return;
                    }
                    $item = $this->createMatchFromXmlNode($node);
                    if ($item->getCompetitionSeason()->getId() !== $competitionSeasonId) {
                        //Not from the appropriate competitionSeason
                        return;
                    }
                } catch (MappingException $e) {
                    $this->getLogger()->warning('Mapping error in match: ' . $id . ' - ' . $e->getMessage());
                    $pending = PendingCollection::getInstance();
                    $pending->addFromMapping($e, $this->getFilePath());
                    $this->getLogger()->warning('Mapping error added to pending - ' . $e->getMessage(),
                        [$this->getLogger()::CHANNEL_EDITORIAL]
                    );
                    return;
                } catch (MissingItemException $e) {
                    $this->getLogger()->warning('Missing item error in match: ' . $id . ' - ' . $e->getMessage());
                    return;
                } catch (EmptyItemException $e) {
                    $this->getLogger()->warning('Empty item error in match: ' . $id . ' - ' . $e->getMessage());
                    return;
                }
                $item->setId($mappingCollection->get($mappingCollection::ENTITY_MATCH, self::PROVIDER, $id));
                $result->addRegistered($item);
            });
        return $result;
    }

    /**
     * @param Crawler $node
     * @return Match
     * @throws MappingException
     * @throws MissingItemException
     * @throws EmptyItemException
     * @throws Exception
     */
    protected function createMatchFromXmlNode(Crawler $node): Match
    {
        $mappingCollection = MappingCollection::getInstance();
        $xmlMatchInfo = $node->filterXPath('MatchData/MatchInfo');
        if ($xmlMatchInfo->count() === 0) {
            throw new Exception('There is no node matchInfo, xml malformed');
        }
        $idProvider = $node->attr('uID');
        $match = new Match();
        //Date
        $data = $xmlMatchInfo->filterXPath('MatchInfo/Date');
        if ($data->count() > 0) {
            try {
                $date = new Date($data->text(), new DateTimeZone(self::DATETIME_ZONE));
                $match->setDate($date->getTimestamp());
                //Date confirmed: if no tbc attr it is confirmed
                if (empty($data->attr('TBC')))
                    $match->setDateConfirmed(true);
                else
                    $match->setDateConfirmed(false);
            } catch (Exception $e) {
                //Do nothing, date will not be set
            }
        }
        //Stage - Without stage mapping we can not do anything
        $stageId = $this->generateProviderStageId(
            $this->getCompetitionSeasonFromXml(),
            $xmlMatchInfo->attr('RoundType'),
            $xmlMatchInfo->attr('RoundNumber')
        );
        $match->setStageById($mappingCollection->get($mappingCollection::ENTITY_STAGE, self::PROVIDER, $stageId));
        //CompetitionSeason
        $match->setCompetitionSeasonById(
            $this->getCompetitionSeasonIdFromStageId(
                $mappingCollection->get($mappingCollection::ENTITY_STAGE, self::PROVIDER, $stageId)
            )
        );
        //Group
        if (!empty($xmlMatchInfo->attr('GroupName'))) {
            $match->setGroupNumber($this->generateGroupNumber($xmlMatchInfo->attr('GroupName')));
        }
        //State
        $match->setStateById($mappingCollection->get(
            $mappingCollection::ENTITY_MATCH_STATE,
            self::PROVIDER,
            $xmlMatchInfo->attr('Period')
        ));
        //ChartOrder
        if (isset($this->matchesChartOrder[$idProvider])) {
            $match->setChartOrder($this->matchesChartOrder[$idProvider]);
        }
        //Leg
        if (!empty($xmlMatchInfo->attr('Leg'))) {
            $match->setLeg($this->generateLegNumber($xmlMatchInfo->attr('Leg')));
        }
        //FirstLeg
        if (!empty($xmlMatchInfo->attr('FirstLegId'))) {
            if ($mappingCollection->exists(
                $mappingCollection::ENTITY_MATCH,
                self::PROVIDER,
                $xmlMatchInfo->attr('FirstLegId'))) {
                $match->setPreviousMatchById($mappingCollection->get(
                    $mappingCollection::ENTITY_MATCH,
                    self::PROVIDER,
                    $xmlMatchInfo->attr('FirstLegId')));
            }
        }
        //MatchDay
        //Searching for the first match day of that stage
        $firstMatchDayStage = null;
        $queryTmp = 'MatchInfo';
        if (!is_null($xmlMatchInfo->attr('RoundType')))
            $queryTmp .= '[@RoundType="' . $xmlMatchInfo->attr('RoundType') . '"]';
        if (!is_null($xmlMatchInfo->attr('RoundNumber')))
            $queryTmp .= '[@RoundNumber="' . $xmlMatchInfo->attr('RoundNumber') . '"]';
        $query = '//' . $queryTmp . '/@MatchDay[not(. > ../' . $queryTmp . '/@MatchDay)][1]';
        $firstMatchDayStage = $this->getCrawledXmlDocument()->filterXPath($query);
        if ($firstMatchDayStage->count() > 0) {
            $firstMatchDayStage = $firstMatchDayStage->text();
        }
        if (is_null($xmlMatchInfo->attr('MatchDay'))) {
            throw new EmptyItemException('MatchDay', 'provider xml');
        }
        $match->setMatchDay($this->getRealMatchDay($xmlMatchInfo->attr('MatchDay'), $firstMatchDayStage));
        //Winner
        if (!empty($xmlMatchInfo->attr('MatchWinner'))) {
            if ($mappingCollection->exists(
                $mappingCollection::ENTITY_TEAM,
                self::PROVIDER,
                $xmlMatchInfo->attr('MatchWinner')
            )) {
                $match->setWinnerById($mappingCollection->get(
                    $mappingCollection::ENTITY_TEAM,
                    self::PROVIDER,
                    $xmlMatchInfo->attr('MatchWinner')));
            }
        }
        //Classified
        if (!empty($xmlMatchInfo->attr('LegWinner'))) {
            if ($mappingCollection->exists(
                $mappingCollection::ENTITY_TEAM,
                self::PROVIDER,
                $xmlMatchInfo->attr('LegWinner')
            )) {
                $match->setClassifiedById($mappingCollection->get(
                    $mappingCollection::ENTITY_TEAM,
                    self::PROVIDER,
                    $xmlMatchInfo->attr('LegWinner')));
            }
        }
        //Incomplete data, we only set it if detail level is appropriate
        if ($mappingCollection->exists($mappingCollection::ENTITY_MATCH, self::PROVIDER, $idProvider)) {
            $id = $mappingCollection->get($mappingCollection::ENTITY_MATCH, self::PROVIDER, $idProvider);
        }
        if (!isset($id) || !isset($this->getMatches()[$id]) || in_array($this->getMatches()[$id]->getDetail()->getId(), self::DETAILS_ACCEPTED)) {
            $match = $this->addIncompleteDataMatchFromXmlNode($node, $match);
        }
        return $match;
    }

    /**
     * @param Crawler $node
     * @param Match $match
     * @return Match
     * @throws MappingException
     * @throws Exception
     */
    protected function addIncompleteDataMatchFromXmlNode(Crawler $node, Match &$match): Match
    {
        $mappingCollection = MappingCollection::getInstance();
        //Detail
        $match->setDetailById(CategoryInterface::MATCH_DETAIL_BASIC_GOALS);
        //Teams
        $xmlMatchInfo = $node->filterXPath('MatchData/MatchInfo');
        if ($xmlMatchInfo->count() === 0) {
            throw new Exception('There is no node matchInfo, xml malformed');
        }
        $preKnownTeams = new PreKnownTeams();
        $teamXml = $node->filterXPath('MatchData/TeamData[@Side="Home"]');
        if ($teamXml->count() > 0) {
            try {
                $match->setLocal($this->createMatchTeamFromXmlNode($teamXml));
            } catch (MappingException $e) {
                if ($e->getType() === $mappingCollection::ENTITY_TEAM) {
                    //Insert pre known team
                    $preKnownTeams->setLocal($this->getPreKnownTeamFromXmlNode($teamXml));
                }
            }
        }
        $teamXml = $node->filterXPath('MatchData/TeamData[@Side="Away"]');
        if ($teamXml->count() > 0) {
            try {
                $match->setVisitor($this->createMatchTeamFromXmlNode($teamXml));
            } catch (MappingException $e) {
                if ($e->getType() === $mappingCollection::ENTITY_TEAM) {
                    //Insert pre known team
                    $preKnownTeams->setVisitor($this->getPreKnownTeamFromXmlNode($teamXml));
                }
            }
        }
        //Insert pre known teams
        if (!is_null($preKnownTeams->getLocal() || !is_null($preKnownTeams->getVisitor()))) {
            $match->setPreKnownTeams($preKnownTeams);
        }
        //Venue
        if ($mappingCollection->exists(
            $mappingCollection::ENTITY_VENUE,
            self::PROVIDER,
            $xmlMatchInfo->attr('Venue_id')
        )) {
            $match->setVenueById($mappingCollection->get(
                $mappingCollection::ENTITY_VENUE,
                self::PROVIDER,
                $xmlMatchInfo->attr('Venue_id')
            ));
        }
        //Referees
        $referees = array();
        $node->filterXPath('MatchData/MatchOfficials/MatchOfficial')
            ->each(function (Crawler $node) use (&$referees) {
                try {
                    $personRol = $this->createMatchRefereeFromXmlNode($node);
                    $referees[] = $personRol;
                } catch (MappingException $e) {
                    $this->getLogger()->notice(
                        'Can not insert referee due to mapping: ' . $e->getMessage(),
                        [$this->getLogger()::CHANNEL_EDITORIAL]
                    );
                    //Do nothing, we do not set this referee because we do not have his mapping
                }
            });
        if (!empty($referees)) {
            $match->setReferees($referees);
        }
        return $match;
    }

    /**
     * @param Crawler $node
     * @return PersonRol
     * @throws MappingException
     */
    protected function createMatchRefereeFromXmlNode(Crawler $node): PersonRol
    {
        $mappingCollection = MappingCollection::getInstance();
        $personRol = new PersonRol();
        $personRol->setPersonById($mappingCollection->get(
            $mappingCollection::ENTITY_PERSON,
            self::PROVIDER,
            $node->attr('uID')
        ));
        $personRol->setRolById($mappingCollection->get(
            $mappingCollection::ENTITY_ROL,
            self::PROVIDER,
            $node->attr('Type')
        ));
        return $personRol;
    }

    /**
     * @param Crawler $node
     * @return Team
     * @throws MappingException
     */
    protected function createMatchTeamFromXmlNode(Crawler $node): Team
    {
        $mappingCollection = MappingCollection::getInstance();
        $team = new Team();
        $team->setTeamById($mappingCollection->get(
            $mappingCollection::ENTITY_TEAM,
            self::PROVIDER,
            $node->attr('TeamRef')
        ));
        //Goals
        $goals = new Goals();
        $goals->setScore($node->attr('Score'));
        $goalsDetail = array();
        $node->filterXPath('TeamData/Goal[not(@Period="ShootOut")]')
            ->each(function (Crawler $goalXml) use ($mappingCollection, &$goalsDetail): bool {
                try {
                    $goal = new Goal();
                    $goal->setPlayerById($mappingCollection->get(
                        $mappingCollection::ENTITY_PERSON,
                        self::PROVIDER,
                        $goalXml->attr('PlayerRef'))
                    );
                    $this->setGoalType($goal, $goalXml->attr('Type'));
                    //Period
                    if ($mappingCollection->exists(
                        $mappingCollection::ENTITY_MATCH_PERIOD,
                        self::PROVIDER,
                        $goalXml->attr('Period')
                    )) {
                        $moment = new Moment();
                        $moment->setPeriodById($mappingCollection->get(
                            $mappingCollection::ENTITY_MATCH_PERIOD,
                            self::PROVIDER,
                            $goalXml->attr('Period'))
                        );
                        $goal->setMoment($moment);
                    }
                    $goalsDetail[] = $goal;
                } catch (MappingException $e) {
                    $this->getLogger()->notice(
                        'Can not insert goals due to mapping: ' . $e->getMessage(),
                        [$this->getLogger()::CHANNEL_EDITORIAL]
                    );
                    //Reset goals
                    $goalsDetail = array();
                    //Break
                    return false;
                }
                return true;
            });
        //We add this always although it may be empty because maybe there has been an error and we have to eliminate
        $goals->setDetails($goalsDetail);
        $team->setGoals($goals);
        //Penalties
        $penalties = new PenaltiesShootOut();
        $penalties->setScore($node->attr('PenaltyScore'));
        $penaltiesDetail = array();
        $node->filterXPath('TeamData/Goal[@Period="ShootOut"]')
            ->each(function (Crawler $goalXml) use ($mappingCollection, &$penaltiesDetail) : bool {
                try {
                    $goal = new PenaltyShootOut();
                    $goal->setPlayerById($mappingCollection->get(
                        $mappingCollection::ENTITY_PERSON,
                        self::PROVIDER,
                        $goalXml->attr('PlayerRef'))
                    );
                    //Shoot
                    //Always scored because they do not send the others
                    $goal->setShootById(CategoryInterface::PENALTY_SHOOT_SCORED);
                    //Period
                    if ($mappingCollection->exists(
                        $mappingCollection::ENTITY_MATCH_PERIOD,
                        self::PROVIDER,
                        $goalXml->attr('Period')
                    )) {
                        $moment = new Moment();
                        $moment->setPeriodById($mappingCollection->get(
                            $mappingCollection::ENTITY_MATCH_PERIOD,
                            self::PROVIDER,
                            $goalXml->attr('Period'))
                        );
                        $goal->setMoment($moment);
                    }
                    $penaltiesDetail[] = $goal;
                } catch (MappingException $e) {
                    $this->getLogger()->notice(
                        'Can not insert penalties due to mapping: ' . $e->getMessage(),
                        [$this->getLogger()::CHANNEL_EDITORIAL]
                    );
                    //Reset penalties
                    $penaltiesDetail = array();
                    //Break
                    return false;
                }
                return true;
            });
        //We add this always although it may be empty because maybe there has been an error and we have to eliminate
        $penalties->setDetails($penaltiesDetail);
        $team->setPenaltiesShoutOut($penalties);
        return $team;
    }

    /**
     * @param Crawler $node
     * @return Category
     * @throws MissingItemException
     * @throws MappingException
     */
    protected function getPreKnownTeamFromXmlNode(Crawler $node): Category
    {
        $data = $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/Team[@uID="' . $node->attr('TeamRef') . '"]/Name');
        if ($data->count() <= 0) {
            throw new MissingItemException($node->attr('TeamRef'), 'Team', $this->getFilePath());
        }
        $mappingCollection = MappingCollection::getInstance();
        $category = new Category();
        $category->setId($mappingCollection->get(
            $mappingCollection::ENTITY_PRE_KNOWN_TEAM,
            self::PROVIDER,
            $data->text()
        ));
        return $category;
    }

    /**
     * @return array
     * @throws EmptyItemException
     * @throws Exception
     */
    protected function getMatchesChartOrder(): array
    {
        $matches = array();
        $isChart = $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/MatchData/MatchInfo[@NextMatch and @RoundType="Semi-Finals"]')
            ->count();
        //It is a competition without chart
        if ($isChart === 0) {
            throw new EmptyItemException('chartOrder', $this->getFilePath());
        }
        //We start with the matches of the final (final, 3th and 4th place ... etc) because they all have a fixed order
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/MatchData/MatchInfo[@RoundType="Final"]')
            ->each(function (Crawler $node) use (&$matches) {
                $matches[$node->parents()->attr('uID')] = 1;
            });
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/MatchData/MatchInfo[@RoundType="3rd and 4th Place"]')
            ->each(function (Crawler $node) use (&$matches) {
                $matches[$node->parents()->attr('uID')] = 1;
            });
        //Rest of the stages. ORDER IS IMPORTANT. It must be reversed because a chart is built from the final
        $stagesName = ['Semi-Finals', 'Quarter-Finals', 'Round of 16', 'Round of 32'];
        foreach ($stagesName as $stageName) {
            $this->getCrawledXmlDocument()
                ->filterXPath('SoccerFeed/SoccerDocument/MatchData/MatchInfo[@RoundType="' . $stageName . '"]')
                ->each(
                    function (Crawler $node) use (&$matches) {
                        $nextMatch = $node->attr('NextMatch');
                        if (!isset($matches[$nextMatch])) {
                            throw new MissingItemException((string)$nextMatch, 'Match', 'chartOrder in file: ' . $this->getFilePath());
                        }
                        //Calculation of position. If a match is in a x position in a future round in the previous
                        //is in position x*2 or x*2-1 (depending if in the future round is in home or away position).
                        //example: if in semifinals is in position 2, in quarters is coming from the matches in position 3 or 4
                        $position = $matches[$nextMatch] * 2;
                        if ($node->attr('NextMatchPosition') === 'Home') {
                            $position--;
                        }
                        $matches[$node->parents()->attr('uID')] = $position;
                    });
        }
        return $matches;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingStagesFromXml(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_STAGE, self::PROVIDER);
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/MatchData/MatchInfo')
            ->each(function (Crawler $node) use ($collection) {
                $stageId = $this->generateProviderStageId(
                    $this->getCompetitionSeasonFromXml(),
                    $node->attr('RoundType'),
                    $node->attr('RoundNumber'));
                $collection->addId($stageId);
            });
        return $collection;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingMatchesFromXml(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_MATCH, self::PROVIDER);
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/MatchData[@uID]')
            ->each(function (Crawler $node) use ($collection) {
                $collection->addId($node->attr('uID'));
            });
        return $collection;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingTeamsFromXml(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_TEAM, self::PROVIDER);
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/MatchData/TeamData[@TeamRef]')
            ->each(function (Crawler $node) use ($collection) {
                $collection->addId($node->attr('TeamRef'));
            });
        return $collection;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingPeopleFromXml(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_PERSON, self::PROVIDER);
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/MatchData/TeamData/Goal[@PlayerRef]')
            ->each(function (Crawler $node) use ($collection) {
                $collection->addId($node->attr('PlayerRef'));
            });
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/MatchData/MatchOfficials/MatchOfficial[@uID]')
            ->each(function (Crawler $node) use ($collection) {
                $collection->addId($node->attr('uID'));
            });
        return $collection;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingVenuesFromXml(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_VENUE, self::PROVIDER);
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/MatchData/MatchInfo[@Venue_id]')
            ->each(function (Crawler $node) use ($collection) {
                $collection->addId($node->attr('Venue_id'));
            });
        return $collection;
    }

    /**
     * @return bool
     */
    protected function matchesHaveRelatedMatchesIds(): bool
    {
        return (
            $this->getCrawledXmlDocument()->filterXPath('SoccerFeed/SoccerDocument/MatchData/MatchInfo/@FirstLegId')->count() > 0
            || $this->getCrawledXmlDocument()->filterXPath('SoccerFeed/SoccerDocument/MatchData/MatchInfo/@MatchWinner')->count() > 0
            || $this->getCrawledXmlDocument()->filterXPath('SoccerFeed/SoccerDocument/MatchData/MatchInfo/@LegWinner')->count() > 0
        );
    }
}